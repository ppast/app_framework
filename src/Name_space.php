<?php

namespace Ppast\App_Framework;



// classe de description d'un namespace
class Name_space
{
	// [---- DECL. STATIQUES ----

	/**
	 * Créer un namespace depuis une représentation "app_ctx_page"
	 *
	 * @param string $ns
	 * @return null|Name_space Renvoie null si $ns n'est pas décomposable en app_ctx_page, sinon un objet Name_space
	 */
	public static function fromString($ns)
	{
	    $regs = [];
		if ( preg_match('~([a-zA-Z0-9]+)_([a-zA-Z0-9]+)(?:_([a-zA-Z0-9_-]+))?~', $ns, $regs) )
			return new Name_space($regs[1], $regs[2], $regs[3]);
		
		return NULL;
	}

	// ---- DECL. STATIQUES ----]


	// [---- DECL. PUBLIQUES ----
	
	public $app = NULL;
	public $ctx = NULL;
	public $page = NULL;
	
	
	/**
	 * Constructeur
	 * 
	 * @param string $app
	 * @param string $ctx
	 * @param string $page
	 */
	public function __construct($app, $ctx, $page)
	{
		$this->app = $app;
		$this->ctx = $ctx;
		$this->page = is_null($page)?'':$page;
	}
	
	
	
	/**
	 * Obtenir namespace sous la forme app/ctx/page
	 *
	 * @return string
	 */
	public function toPage()
	{
		return $this->app . '/' . $this->ctx . '/' . $this->page;
	}
	
	
	
	/**
	 * Obtenir namespace sous la forme app_ctx_page
	 *
	 * @return string
	 */
	public function toString()
	{
		return $this->app . '_' . $this->ctx . '_' . $this->page;
	}


	
	/**
	 * Obtenir namespace sous la forme app (tronquer et ne renvoyer que la partie application)
	 *
	 * @return string
	 */
	public function toAppPath()
	{
		return $this->app;
	}


	
	/**
	 * Obtenir namespace sous la forme app/ctx (tronquer et ne renvoyer que la partie application et contexte)
	 *
	 * @return string
	 */
	public function toCtxPath()
	{
		return $this->app . '/' . $this->ctx;
	}
	

	
	/**
	 * Obtenir namespace sous la forme app\ctx\page (format des namespaces PHP)
	 *
	 * @return string
	 */
	public function toNamespace()
	{
		return str_replace('/', '\\', $this->toPage()) . '\\';
	}
	

	
	/** 
	 * Convertir un namespace app/ctx/script en appCtxScript
	 *
	 * @return string
	 */
	public function toCamelCase()
	{
		return Utils::camelCase(explode("/", $this->toPage()));
	}
	
	// ---- DECL. PUBLIQUES ----}
}

?>