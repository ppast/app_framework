<?php

namespace Ppast\App_Framework\FeedbackHandlers;

use \Ppast\App_Framework\Request;



// classe pour gérer feedback
abstract class Handler
{
	/**
	 * Afficher le feedback
	 *
	 * @param \Ppast\App_Framework\Request $req
	 */
	abstract function display(Request $req);
}



?>