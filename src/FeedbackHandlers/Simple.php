<?php

namespace Ppast\App_Framework\FeedbackHandlers;

use \Ppast\App_Framework\Request;



// classe par défaut pour gérer feedback
class Simple extends Handler
{
	// formatage par défaut
	const FB_OK = "background-color:#009900; color:#FFFFFF; font-weight:bold; padding:5px; border:2px solid darkgreen; margin-bottom:1em;";
	const FB_KO = "background-color:#FF0000; color:#FFFFFF; font-weight:bold; padding:5px; border:2px solid darkred; margin-bottom:1em;";
	
	
	/**
	 * Afficher le feedback
	 *
	 * @param \Ppast\App_Framework\Request $req
	 */
	function display(Request $req)
	{
		// si feedback renvoyé
		if ( $req->testFeedBack() )
		{
			$msg = implode(',', $req->getFeedback());
			echo "<div id=\"app_frmk_feedback_line\" style=\"" . ($req->getCommand()->failed() ? self::FB_KO : self::FB_OK) . "\">$msg</div>";
			
			// si commande OK, faire disparaitre le message au bout de quelques instants
			if ( !$req->getCommand()->failed() )
			{
				echo "<script language='javascript'>";
				echo "window.setTimeout('";
				echo 	"document.getElementById(\"app_frmk_feedback_line\").style.visibility = \"hidden\";";
				echo 	"document.getElementById(\"app_frmk_feedback_line\").style.display = \"none\";";			
				echo "', 4000);";
				echo "</script>";
			}
		}
	}
}


?>