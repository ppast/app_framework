<?php

namespace Ppast\App_Framework;



// classe requete
// en URL : __namespace__ = desk_prestations_clients
//          __command__ = reload
class Request
{
	// --- DECL. PRIVEES ---
	private $_browserInterface = NULL;
	private $_fileUploadsInterface = NULL;
	private $_feedback = array();
	private $_returns = array();
	
	private $_namespace;
	private $_command;
	private $_commandHandler;
	// --- /DECL. PRIVEES ---
	
		
	// --- DECL. STATIQUES ---
	const REQUEST_NAMESPACE = "__namespace__";
	const REQUEST_COMMAND = "__command__";
	// --- /DECL. STATIQUES ---
	
	
	
	/**
	 * Constructeur
	 *
	 * @param \Ppast\App_Framework\Sessions\Session $session Objet de session courant	 
	 */
	function __construct(Sessions\Session $session)
	{
		// init
		$this->init($session);
	}
	
	
	
	/**
	 * Initialiser la requête avec la session courante
	 *
	 * @param \Ppast\App_Framework\Sessions\Session $session Objet de session courant	 
	 */
	function init(Sessions\Session $session)
	{
		$this->_browserInterface = $session->getBrowserRequestInterface();
		$this->_fileUploadsInterface = $session->getFileUploadsRequestInterface();
		$this->_namespace = $this->_browserInterface->{self::REQUEST_NAMESPACE};
		$this->_command = $this->_browserInterface->{self::REQUEST_COMMAND};
	}
	
	
	
	/**
	 * Obtenir une instance FileUploadRequest décrivant un fichier envoyé depuis le navigateur
	 * 
	 * @param string $k Nom du téléchargement dans le body de la requête envoyée par le navigateur
	 * @return FileUploadRequest
	 */
	function getFileUpload($k)
	{
		return $this->_fileUploadsInterface->{$k};
	}
	
	
	
	/**
	 * Obtenir les fichiers envoyés dans la requête dans une liste d'instances FileUploadRequest
	 *
	 * @return FileUploadRequest[]
	 */
	function getFileUploads()
	{
		return $this->_fileUploadsInterface->toArray();
	}
	
	
	
	/**
	 * Déplacer un fichier téléchargé
	 *
	 * @param string $k Nom du fichier envoyé dans le body de la requête depuis le navigateur
	 * @param string $dest Chemin complet pour nouveau fichier
	 * @return bool Renvoie true si le fichier temporaire a pu être déplacé
	 */
	function moveUploadedFile($k, $dest)
	{
		if ( is_string($k) )
			$k = $this->getFileUpload($k);
			
		if ( !$k )
			return false;
			
		return $this->_fileUploadsInterface->moveUploadedFile($k, $dest);
	}
	
	
	
	/**
  	 * Tester existence de fichiers téléchargés dans le body de la requête envoyée par le navigateur
	 *
	 * @param string[] Liste de noms de champs du body
	 * @return bool Renvoie true si tous les champs demandés correspondent à des fichiers envoyés
	 */
	function testFileUploads($k)
	{
		if ( is_array($k) )
		{
			foreach ( $k as $i )
				// si paramètre non précisé, on renvoie false
				if ( is_null($this->_fileUploadsInterface->{$i}) )
					return false;
					
			// si on arrive ici, ts les paramètres nécessaires ont été transmis
			return true;
		}
		else
			return !is_null($this->_fileUploadsInterface->{$k});
	}
	
	
	
	/**
	 * Acccesseur aux valeurs de requête extraites depuis l'interface navigateur ou aux propriétés _namespace et _commande
	 *
	 * @param string $k Propriété à lire (Request::REQUEST_NAMESPACE ou Request::REQUEST_COMMAND) ou nom de valeur dans le body de la requête	 
	 * @return string
	 */
	function get($k)
	{
		// accesseur optimisé
		switch ( $k )
		{
			case self::REQUEST_NAMESPACE :
				return $this->_namespace;
				break; 
			case self::REQUEST_COMMAND :
			case 'command' :
				return $this->_command;
				break; 
			default:
				return $this->_browserInterface->{$k};
		}
	}
	
	
	
	/**
	 * Définir une valeur dans le body de la requête
	 * 
	 * @param string $k
	 * @param string $v
	 */
	function set($k, $v)
	{
		return $this->_browserInterface->{$k} = $v;
	}
	
	
		
	/** 
	 * Accesseur magique
	 * 
	 * @param string $k
	 * @return string
	 */
	function __get($k) { return $this->get($k); }

	
	/** 
	 * Accesseur magique
	 * 
	 * @param string $k
	 * @param string $v
	 */
	function __set($k, $v) { $this->set($k, $v); }


	
	/**
	 * Définir message (succès ou erreur) 
	 *
	 * @param string $s Message à destination de l'utilisateur
	 * @param null|int $status Code de retour
	 * @return Request|int Renvoie $this si $status n'est pas précisé (chaînage) ou la valeur int $status si cet argument est précisé
	 */
	function feedback($s, $status = NULL)
	{
		if ( !is_null($s) )
			if ( is_array($s) )
				foreach ( $s as $fb )
					$this->_feedback[] = $fb;
			else
				$this->feedback(array($s));
			
		// après avoir défini un message, renvoyer le statut passé en arguement (permet chainage dans l'appelant)
		if ( is_null($status) )
			return $this;
		else
			return $status;
	}
	
	
	
	/**
	 * Tester si la requête a des message de feedback renvoyés
	 *
	 * @return int Renvoie 0 si aucun message, sinon le nombre de messages
	 */
	function testFeedBack()
	{
		return count($this->_feedback);
	}
	
	
	
	/**
	 * Obtenir liste des messages feedback
	 *
	 * @return string[]
	 */
	function getFeedback()
	{
		return $this->_feedback;
	}
	
	
	
	/**
	 * Effacer les messages feedback
	 */
	function deleteFeedback()
	{
		$this->_feedback = array();
	}
	
	
	
	/**
	 * Renvoyer une valeur 
	 * 
	 * @param string $k Nom de valeur
	 * @param mixed $o Valeur de retour
	 * @return Request Renvoie $this pour chaînage
	 */
	function setReturn($k, $o)
	{
		$this->_returns[$k] = $o;
		return $this;
	}
	
	
	
	/**
	 * Obtenir une valeur de retour
	 * 
	 * @param string $k Nom de valeur de retour
	 * @return mixed
	 */
	function getReturn($k)
	{
		if ( array_key_exists($k, $this->_returns) )
			return $this->_returns[$k];
		else
			return null;
	}
	
	
	
	/**
	 * Obtenir le tableau associatif de toutes les valeurs de retour
	 *
	 * @return array
	 */
	function getReturns()
	{
		return $this->_returns;
	}
	
	
	
	/**
	 * Tester existence valeurs de retour
	 *
	 * @param string|string[] Nom de la valeur de retour à tester ou liste de noms de valeurs de retour
	 * @return bool Renvoie true si toutes les valeurs demandées sont définies
	 */
	function testReturn($k)
	{
		if ( is_array($k) )
		{
			foreach ( $k as $i )
			{
				// si paramètre non précisé, on renvoie false
				if ( !array_key_exists($i, $this->_returns) )
					return false;
			}
					
			// si on arrive ici, ts les paramètres nécessaires ont été transmis
			return true;
		}
		else
			return array_key_exists($k, $this->_returns);
	}
	
	
	
	/**
	 * Obtenir l'instance Command associée à la requête
	 *
	 * @return Command
	 */
	function getCommand()
	{
		return $this->_commandHandler;
	}
	
	
	
	/** 
	 * Définir l'instance Command traitant cette requête
	 *
	 * @param Command $cmd
	 * @return Request Renvoie $this pour chaînage
	 */
	function setCommand($cmd)
	{
		$this->_commandHandler = $cmd;
		return $this;
	}
	
	
	
	/**
	 * Modifier les paramètres d'une requête
	 * 
	 * - Généralement effectué en vue de transférer le traitement à une autre commande, qui utilise
	 * des paramètres différents
	 * - Pour remplacer un paramètre par un autre, simplement indiquer dans la matrice de transformation le nom du nouveau paramètre : $tmatrix['old_key'] = 'new_key'
	 * - Pour modifier une valeur avant de la transférer, indiquer un tableau [callback(Request $req, string $key, $param1, $param2, ...), $params]
	 *   dans la matrice de transformation, où $param est une liste de paramètres supplémentaires dont le callback pourrait avoir besoin pour fonctionner
	 * 
	 * @param array Tableau associatif décrivant la transformation
	 * @return Request Renvoie $this pour chaînage
	 */
	function transform($tmatrix)
	{
		foreach ( $tmatrix as $nkey=>$key )
			// si remplacement simple d'un nom de paramètre par un autre
			if ( is_string($key) )
				$this->_browserInterface->{$nkey} = $key;
				
			// si fonction de remplacement
			else if ( is_array($key) && (count($key) >= 1) )
			{
				// paramètres externes existent ? si non, créer un tableau vide, puis ajouter en début REQUEST et NKEY
				$key[1] = !empty($key[1]) ? $key[1] : array();
				array_unshift($key[1], $this, $nkey);
				$this->_browserInterface->{$nkey} = call_user_func_array($key[0], $key[1]);
			}
			
		return $this;
	}
	
	
	
	/**
	 * Tester existence de valeurs dans le body de la requête
	 * 
	 * @param string|string[] $k Nom de valeur ou liste de noms de valeurs à tester
	 * @return bool Renvoie true si toutes les valeurs demandées existents
	 */
	function test($k)
	{
		if ( is_array($k) )
		{
			foreach ( $k as $i )
				// si paramètre non précisé, on renvoie false
				if ( is_null($this->_browserInterface->{$i}) )
					return false;
					
			// si on arrive ici, ts les paramètres nécessaires ont été transmis
			return true;
		}
		else
			return !is_null($this->_browserInterface->{$k});
	}
	
	
	
	/**
	 * Obtenir un tableau des valeurs de requête
	 * 
	 * @return array Renvoie le tableau associatif (nom, valeur) des paramètres de requête
	 */
	function getItems()
	{
		return $this->_browserInterface->toArray();
	}
}

?>