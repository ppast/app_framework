<?php

namespace Ppast\App_Framework;



// classe abstraite pour commande
abstract class Command
{
	// --- CONSTANTES ---
	const CMD_INACTIVE = 0;
	const CMD_OK = 1;
	const CMD_ERROR = 2;
	const CMD_INSUFFICIENT_DATA = 3;
	const CMD_NOT_FOUND = 4;
	// --- /CONSTANTES ---
	
	
	// --- DECL. PRIVEES ---
	private $_status = NULL;
	private $_outputHandler = NULL;
	private $_halt = false;
	// /--- DECL. PRIVEES ---


	/**
	 * Constructeur
	 * 
	 * @param string $ohandler Nom de la méthode de rendu à utiliser (héritant de OutputHandlers\Handler) pour cette commande
	 */
	function __construct($ohandler = StdoutController::OUTPUT_PHP)
	{
		$this->_status = self::CMD_INACTIVE;
		$this->setOutputHandler($ohandler);
	}
	
	

	/** 
	 * Obtenir statut commande
	 *
	 * @return int
	 */
	function getStatus()
	{
		return $this->_status;
	}
	
	
	
	/**
	 * Tester si la commande a échoué
	 * 
	 * @return bool Renvoie true si le statut de retour est différent de CMD_OK
	 */
	function failed()
	{
		return $this->_status != self::CMD_OK;
	}
	
	
	
	/**
	 * Tester/définir si on doit arrêter le traitement (cas des commandes xmlhttp et download) en fonction de l'argument booléen
	 * 
	 * @param null|bool $b Si $b vaut NULL, on interroge, la valeur de $this->_halt ; si $b est booléen, définir $this->_halt avec $b
	 * @return bool
	 */	 
	function halt($b = NULL)
	{
		if ( is_null($b) )
			return $this->_halt;
		else
			$this->_halt = $b;
	}
	
	
	
	/**
	 * Teste si cette commande est exécutable sur une requête (paramètres nécessaires fournis)
	 *
	 * @param Request $req
	 * @return bool
	 */
	function isValid(Request $req)
	{
		return true; // par défaut, oui
	}
	
	

	/**
	 * Exécute la commande et renvoie un code de statut
	 *
	 * @param Request $req
	 * @return int
	 */
	function execute(Request $req)
	{
		// mémoriser la commande traitant
		$req->setCommand($this);

		// si valide, on exécute
		if ( $this->isValid($req) )
			$ret = $this->doExecute($req);
		else
			$ret = $req->feedback("Paramètres incorrects", self::CMD_INSUFFICIENT_DATA);
			
		if ( is_null($ret) )
			$ret = $req->feedback("Statut de retour inconnu", self::CMD_ERROR);

			
		// stocker le statut de retour
		return $this->_status = $ret;
	}
	
	
	
	/**
	 * Définir une méthode de rendu
	 *
	 * @param string $oh Nom de classe héritant de OutputHandlers\Handler
	 * @return OutputHandlers\Handler Renvoie la classe instanciée
	 */
	function setOutputHandler($oh)
	{
		return $this->_outputHandler = new $oh();
	}
	
	
	
	/**
	 * Obtenir la méthode de rendu actuelle
	 *
	 * @return OutputHandlers\Handler Renvoie l'objet de rendu actuellement instancié
	 */
	function getOutputHandler()
	{
		return $this->_outputHandler;
	}
	
	
	
	/**
	 * Méthode abstraite contenant le code réel de la commande
	 *
	 * @param Request $req
	 * @return int Renvoie un code de statut
	 */
	abstract function doExecute(Request $req);
}



?>