<?php

namespace Ppast\App_Framework;



// gestionnaire d'instanciation (commandes, pages)
class ClassResolver
{
	// --- DECL. PRIVEES ---
	protected $_commandClass;
	protected $_classReader;
	protected $_defaultCommandClass;
	protected $_defaultNoCommandClass;
	protected $_pageContainerClass;
	protected $_defaultPageContainerClass;
	protected $_defaultNoCommandPageContainerClass;

	private static $_instance;


	/** 
	 * Constructeur privé
	 *
	 * @param string $ns_prefix Préfixe de namespace pour le projet applicatif
	 */
	protected function __construct($ns_prefix)
	{
		if ( is_null($this->_commandClass) )
		{
			$this->_commandClass = new \ReflectionClass("\Ppast\App_Framework\Command");
			$this->_defaultCommandClass = new \ReflectionClass("\Ppast\App_Framework\DefaultCommand");
			$this->_defaultNoCommandClass = new \ReflectionClass("\Ppast\App_Framework\DefaultNoCommand");
			$this->_pageContainerClass = new \ReflectionClass("\Ppast\App_Framework\PageContainer");
			$this->_defaultPageContainerClass = new \ReflectionClass("\Ppast\App_Framework\DefaultPageContainer");
			$this->_defaultNoCommandPageContainerClass = new \ReflectionClass("\Ppast\App_Framework\DefaultNoCommandPageContainer");
		}
		
		
		// créer classReader
		$this->_classReader = new ClassReader($ns_prefix);
	}
	
	
		
	/**
	 * Normaliser le namespace de cette requete (tout en minuscules)
	 *
	 * @param string $ns
	 * @return string
	 */
	protected function _getNamespace($ns)
	{
		return strtolower($ns);
	}
	
	
	
	/**
	 * Obtenir une instance de la classe traitante pour un namespace et commande demandés
	 *
	 * @param string $ns Namespace courant app_ctx_page sous forme de chaîne
	 * @param string $cl Commande recherchée
	 * @param string $cltype Type de classe sous forme de chaîne (Command ou PageContainer)
	 * @param ReflectionClass $cltypeclass Classe de réflection dont la traitante doit hériter (Ppast\App_Framework\Command ou Ppast\App_Framework\PageContainer)
	 * @return Command|PageContainer|string Renvoie l'objet traitant instancié ou un message d'erreur sinon	 
	 */
	protected function _getClassNs($ns, $cl, $cltype, \ReflectionClass $cltypeclass)
	{
		// obtenir une classe Reflection pour la commande/pagecontainer demandé
		$rclass = $this->_classReader->getClass($ns, $cl, $cltype);
		if ( is_string($rclass) )
			return $rclass;

		
		// sinon vérifier héritage et construire instance
		if ( $rclass->isSubclassOf($cltypeclass) )
			return $rclass->newInstance();
		else
			return "Commande " . $rclass->getName() . " n'hérite pas de " . $cltypeclass->name;
	}
	
	
	
	/**
	 * Obtenir une instance traitante pour la requête en cours
	 *
	 * La classe précise se détermine par le namespace courant et la commande recherchée.
	 * Si pas trouvée, une instance par défaut donnée en paramètre sera renvoyée
	 *
	 * @param string $ns Namespace courant app_ctx_page sous forme de chaîne
	 * @param string $cl Commande recherchée
	 * @param string $cltype Type de classe sous forme de chaîne (Command ou PageContainer)
	 * @param ReflectionClass $cltypeclass Classe de réflection dont la traitante doit hériter (Ppast\App_Framework\Command ou Ppast\App_Framework\PageContainer)
	 * @param ReflectionClass $cldefclass Classe de réflection à instancier et renvoyer si la classe recherchée pour la commande demandée n'existe pas
	 * @param ReflectionClass $cldefnocmdclass Classe de réflection à instancier et renvoyer si la commande n'est pas connu
	 * @param Request $request Objet requête en cours
	 * @return Command|PageContainer|string Renvoie l'objet traitant instancié ou un message d'erreur sinon	 
	 */
	function _getClass($ns, $cl, $cltype, $cltypeclass, $cldefclass, $cldefnocmdclass, Request $request)
	{
		// si commande définie
		if ( !is_null($cl) )
		{
			// tenter d'instancier depuis le namespace fourni 
			$res = $this->_getClassNs($this->_getNamespace($ns), $cl, $cltype, $cltypeclass);

			// si trouvé et instancié, on renvoie l'instance
			if ( is_object($res) )
				return $res;
				
			// si tj pas trouvé, on renvoie la liste des erreurs trouvées
			$request->feedback(array($res));
			
			// et on instancie la commande par défaut 
			return $cldefclass->newInstance();
		}
		
		// si commande pas trouvée, renvoyer commande inexistante par défaut 
		else
			return $cldefnocmdclass->newInstance();
	}
	
	
	// --- /DECL. PRIVEES ---
	
	
	// --- DECL. STATIQUES ---
	
	/**
	 * Obtenir instance singleton
	 *
	 * @param string $ns_prefix Prefixe de namespace pour le projet applicatif
	 * @return ClassResolver
	 */
	static function getInstance($ns_prefix = '')
	{
		if ( !isset(self::$_instance) )
			self::$_instance = new ClassResolver($ns_prefix);
	
				
		return self::$_instance;
	}
	
	
	// --- /DECL. STATIQUES ---
	
	
	/**
	 * Obtenir la page indiquée par un namespace et une chaine
	 *
	 * @param Request $request Objet requête en cours
	 * @param string $ns Namespace actuel sous forme app_ctx_page
	 * @param string $page Nom de page demandé
	 * @return PageContainer|string
	 */
	function getPageContainer(Request $request, $ns, $page)
	{
		return $this->_getClass($ns, $page, 'PageContainer', $this->_pageContainerClass, $this->_defaultPageContainerClass, $this->_defaultNoCommandPageContainerClass, $request);
	}
	
	
	
	/**
	 * Obtenir la page indiquée par la requête, et obtenir éventuellement un container par défaut de la librairie ou du projet
	 *
	 * @param Request $request Objet requête en cours
	 * @return PageContainer|string
	 */
	function getRequestPageContainer(Request $request)
	{
		// tenter d'instancier un container spécifique pour la requête ; si non défini, on renvoie le container par défaut de la librairie
		// ($this->_defaultPageContainerClass) ou le container par défaut pour commande inexistante ($this->_defaultNoCommandPageContainerClass)
		$cl = $this->getPageContainer($request, $request->get(Request::REQUEST_NAMESPACE), $request->get(Request::REQUEST_COMMAND));

		
		// si l'objet renvoyé est le container par défaut de la librairie, tenter d'instancier un container par défaut 
		// pour la page (forgé sur namespace requete)
		if ( $this->_defaultPageContainerClass->isInstance($cl) )
		{
			// tenter d'instancier directement le container par défaut de la page (forgé sur son namespace)
			$cl2 = $this->_getClassNs(	$this->_getNamespace($request->get(Request::REQUEST_NAMESPACE)), 
										'ByDefault' /* commande */,
										'PageContainer' /* cltype */,
										$this->_pageContainerClass) /* PageContainer */;

			
			// si on a pu instancier la classe container par défaut page pour une commande existante, on a renvoyé un objet
			if ( is_object($cl2) )
				return $cl2;
		}
		
		
		// si objet renvoyé est le container par défaut de la librairie, pour une commande inexistante
		// tenter d'instancier un container par défaut dans le namespace de la page
		else if ( $this->_defaultNoCommandPageContainerClass->isInstance($cl) )
		{
			// tenter d'instancier directement le container par défaut de la page (forgé sur son namespace)
			$cl2 = $this->_getClassNs(	$this->_getNamespace($request->get(Request::REQUEST_NAMESPACE)), 
										'NoCommand' /* commande */,
										'PageContainer' /* cltype */,
										$this->_pageContainerClass) /* PageContainer */;
							
			// si on a pu instancier la classe container par défaut page pour une commande existante, on a renvoyé un objet
			if ( is_object($cl2) )
				return $cl2;
		}
	
		
		return $cl;		
	}
	
	
	
	/**
	 * Obtenir la commande indiquée par un namespace et une chaine
	 *
	 * @param Request $request Objet requête en cours
	 * @param string $ns Namespace actuel sous forme app_ctx_page
	 * @param string $cmd Commande demandée
	 * @return Command|string
	 */
	function getCommand(Request $request, $ns, $cmd)
	{
		return $this->_getClass($ns, $cmd, 'Command', $this->_commandClass, $this->_defaultCommandClass, $this->_defaultNoCommandClass, $request);
	}
	
	
	
	/**
	 * Obtenir la commande indiquée par la requête
	 *
	 * @param Request $request Objet requête en cours
	 * @return Command|string
	 */
	function getRequestCommand(Request $request)
	{
		return $this->getCommand($request, $request->get(Request::REQUEST_NAMESPACE), $request->get(Request::REQUEST_COMMAND));
	}
}


?>