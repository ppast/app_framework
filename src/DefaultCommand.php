<?php

namespace Ppast\App_Framework;



// proposer une commande par défaut ; celle-ci renvoie systématiquement un échec
class DefaultCommand extends Command
{
	/**
	 * Méthode contenant le code réel de la commande
	 *
	 * Pour cette commande par défaut, renvoyer un statut d'erreur CMD_NOT_FOUND
	 *
	 * @param Request $req
	 * @return int Renvoie un code de statut
	 */
	function doExecute(Request $req)
	{
		return self::CMD_NOT_FOUND;
	}
}



?>