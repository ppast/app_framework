<?php

namespace Ppast\App_Framework;



final class Utils 
{
	/**
	 * Mettre en capitales les initiales de chaque item et renvoyer une chaine
	 *
	 * @param string[] $arr
	 * @return string
	 */
	static function camelCase(array $arr)
	{
		$ret = "";
		
		foreach ( $arr as $a )
			if ( strlen($a) )
				$ret .= strtoupper(substr($a, 0, 1)) . substr($a, 1);
			
		return $ret;
	}
	
	
	
	/**
	 * Renvoyer la racine web, avec / à la fin
	 *
	 * @return string
	 */
	static function documentRoot()
	{
		return self::ensureTrailingSlash($_SERVER['DOCUMENT_ROOT']);
	}


	
	/**
	 * S'assurer que le dossier se termine par un slash
	 *
	 * @param string $folder
	 * @return string
	 */
	static function ensureTrailingSlash($folder)
	{
		if ( preg_match('~/$~', $folder) )
			return $folder;
		else
			return $folder . '/';
	}
	
	
	
	/**
	 * S'assurer que le chemin ne commence pas par un slash
	 *
	 * @param string $folder
	 * @return string
	 */
	static function ensureNoPrefixSlash($folder)
	{
		if ( preg_match('~^/~', $folder) )
			return substr($folder, 1);
		else
			return $folder;
	}
	
}

?>