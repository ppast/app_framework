<?php

namespace Ppast\App_Framework;



// manifest
class Manifest
{
	// [---- DECL. PROTEGEES ----
	
	// valeurs énumérées du manifest
	protected $_values = NULL;
	

	/**
	 * Constructeur
	 *
	 * @param string $kind Type de manifest (application, contexte, page)
	 * @param array $values Valeurs du manifest, sous forme de tableau associatif clef,valeur
	 */
	protected function __construct($kind, $values)
	{
		$this->kind = $kind;
		$this->_values = $values;
		self::$_last = $this;
	}
	
	// ---- DECL. PROTEGEES ----]

	
	
	// [---- DECL. STATIQUES ----
	
	// type de manifests
	const MANIFEST_APP = 'app';
	const MANIFEST_CONTEXT = 'ctx';
	const MANIFEST_PAGE = 'page';
	
	// valeurs standards pour manifest
	const INCLUDES = 'includes';						// tableau de scripts PHP
	const CSS = 'cssIncludes';							// 1 fichier CSS ou tableau de fichiers CSS
	const JS = 'jsIncludes';							// 1 fichier JS ou tableau de fichiers JS
	const CSS_NO_CACHE = 'cssNoCache';					// tableau de fichiers CSS à ne pas mettre en cache
	const JS_NO_CACHE = 'jsNoCache';					// tableau de fichiers JS à ne pas mettre en cache
	const SECURITY_HANDLERS = 'securityHandlers';		// gestionnaires de sécurité ; tableau ; 1 ligne = nom de classe ou array(nom de classe => array(...parametres...))
	const DEFAULT_CONTEXT = 'defaultContext';			// contexte par défaut modifié ; si absent, extrait de la composante /ctx/ du namespace
	const FEEDBACK_HANDLER = 'feedbackHandler';			// gestionnaire pour traiter le feedback des requêtes ; si absent, c'est au PageContainer de traiter lui-même
	const TEMPLATE = 'template';						// chemin complet depuis racine www de la page modèle
	const JS_INCL_IFCOMMAND = 'jsIncludeIfCommand';		// booleen : n'inclure les JS que si une commande est passée (cas où on n'affiche pas la page métier directement pour économiser la bande passante)
	const APP_URL = 'appUrl';							// URL pour login de l'application ; peut être présente pour app ou ctx
	const APP_ROUTE = 'appRoute';						// URL pour routage ; peut être présent dans app ou ctx
	const APP_DOMAIN = 'appDomain';						// domaine http://www.domain.tld de l'application ; peut être présent dans app ou ctx

	// valeurs standards pour manifest APP
	const MINIFY = 'minify';							// booleen activer le cache ou pas
	const SESSION_CLASS = 'sessionClass';				// classe pour gérer la session
	const APP_NAME = 'appName';							// nom de l'application ; doit être égal à la composante APP du namespace !
	//const APP_INIT_REGISTRIES = 'app_init_registries';	// callback pour initialiser les registres de l'application ; doit renvoyer array(appregistry, connexion db)
	const APP_REGISTRY_PROVIDER = 'appRegistryProvider';	// instance stratégie fournisseur de donnée pour le registre application persistent (généralement, RegistryProviders\DbApplication ou RegistryProviders\JsonFileProvider)
	const RUN_REGISTRY_VALUES = 'runRegistryValues';	// valeurs prédéfinies pour le registre volatile, sans contexte (généralement, l'objet Pdo, pour un accès facile)
	const CONFIG = 'config';							// paramètres de configuration généraux (tableau)
	const APP_NAMESPACE = 'appNamespace';				// prefixe de namespace pour autoload, qui viendra avant la construction app\ctx\page ; si absent, APP_NAME qui est utilisé
	
	// valeurs standards pour manifest CTX
	const SESSION_NAME = 'sessionName';					// nom de la session
	
	// valeurs standards pour manifest PAGE
	const TITLE = 'title';								// chaine titre de la page

	
	
	// dernier manifest lu et cache
	protected static $_last = NULL;
	protected static $_manifestCache = array();
	
	
	
	/**
	 * Lire un manifest depuis un fichier
	 * 
	 * @param string $f Chemin vers fichier manifest
	 */
	protected static function _readFromFile($f)
	{
		// si fichier existe, l'inclure ; la construction déclenchera automatiquement l'assignation de $this créé à $_last
		if ( file_exists($f) )
			include $f;
		else
			self::$_last = NULL;
		
		
		return self::$_last;
	}
	
	
	
	/**
	 * Lire un manifest depuis un fichier json
	 * 
	 * @param string $mtype Type de manifest à lire (app, ctx, ou page)
	 * @param string $f Chemin vers fichier manifest.json
	 */
	protected static function _readFromJson($mtype, $f)
	{
		// si fichier existe
		if ( file_exists($f) )
			return new Manifest($mtype, json_decode(file_get_contents($f), true));
		else
			return null;
	}
	
	
	
	/**
	 * Lire le manifest depuis un fichier, dans le dossier application, contexte ou page
	 *
	 * @param Name_space $ns Objet namespace courant
	 * @param string $mtype Type de manifest à lire (app, ctx, ou page)
	 * @return Manifest
	 */
	protected static function _manifestFromFile(Name_space $ns, $mtype)
	{
		switch ( $mtype )
		{
			// si manifest app
			case self::MANIFEST_APP:
				if ( $m = self::_readFromJson($mtype, Utils::documentRoot() . Bootstrap::$approot . $ns->toAppPath() . '/manifest.json') )
					return $m;
				else
					return self::_readFromFile(Utils::documentRoot() . Bootstrap::$approot . $ns->toAppPath() . '/manifest.php');

			// si manifest ctx
			case self::MANIFEST_CONTEXT:
				if ( $m = self::_readFromJson($mtype, Utils::documentRoot() . Bootstrap::$approot . $ns->toCtxPath() . '/manifest.json') )
					return $m;
				else
					return self::_readFromFile(Utils::documentRoot() . Bootstrap::$approot . $ns->toCtxPath() . '/manifest.php');
			
			// si manifest page
			case self::MANIFEST_PAGE:
				if ( $m = self::_readFromJson($mtype, Utils::documentRoot() . Bootstrap::$approot . $ns->toPage() . '/manifest.json') )
					return $m;
				else
					return self::_readFromFile(Utils::documentRoot() . Bootstrap::$approot . $ns->toPage() . '/manifest.php');
		}

		return NULL;
	}
	
	
	
	/**
	 * Forger une clef pour un namespace et un type de manifest
	 * 
	 * @param Name_space $ns Objet namespace app/ctx/page
	 * @param string $mtype Type de manifest (app, context, ou page)
	 * @return string Renvoie une chaîne qui représente de manière unique le manifest demandé (renvoie app_ctx_page, app_ctx ou app)
	 */
	protected static function _manifestCacheKey(Name_space $ns, $mtype)
	{
		// découper le namespace en tableau
		$n = explode('_', $ns->toString());

		// prendre une partie du tableau, pour constituer un identifiant, selon type du manifest
		switch ( $mtype )
		{
			// si manifest app
			case self::MANIFEST_APP:
				return array_shift($n);
				break;

			// si manifest ctx
			case self::MANIFEST_CONTEXT:
				return implode('_', array_slice($n, 0, 2));
				break;
			
			// si manifest page
			case self::MANIFEST_PAGE:
				return implode('_', $n);
				break;
		}
	}


	
	/**
	 * Lire le manifest depuis le cache
	 *
	 * @param Name_space $ns Objet namespace courant
	 * @param string $mtype Type de manifest à rechercher (app, ctx, page)
	 * @return Manifest
	 */
	protected static function _manifestFromCache(Name_space $ns, $mtype)
	{
		if ( array_key_exists($mtype . '_' . self::_manifestCacheKey($ns, $mtype), self::$_manifestCache) )
			return self::$_manifestCache[$mtype . '_' . self::_manifestCacheKey($ns, $mtype)];
		else
			return null;
	}
	
	
	
	/**
	 * Ecrire le manifest dans le cache
	 *
	 * @param Manifest $m Manifest à stocker
	 * @param Name_space $ns Namespace associé
	 * @return Manifest Renvoie $m
	 */
	protected static function _manifestToCache(Manifest $m, Name_space $ns)
	{
		if ( $m )
			return self::$_manifestCache[$m->kind . '_' . self::_manifestCacheKey($ns, $m->kind)] = $m;
	}
	
	
	
	/** 
	 * Forcer un manifest dans la cache
	 *
	 * @param Manifest $m Manifest à stocker
	 * @param Name_space $ns Namespace associé
	 */
	static function overrideManifest(Manifest $m, Name_space $ns)
	{
		self::_manifestToCache($m, $ns);
	}
	
	
	
	/**
	 * Création d'une instance
	 * 
	 * @param string $k Type de manifest (app, ctx, page)
	 * @param array $values Tableau associatif clef,valeur
	 * @return Manifest
	 */
	static function describe($kind, $values)
	{
		return new Manifest($kind, $values);
	}


	
	/**
	 * Création d'une instance, pour un manifest APP
	 * 
	 * @param array $values Tableau associatif clef,valeur
	 * @return Manifest
	 */
	static function describe_app($values)
	{
		return self::describe(self::MANIFEST_APP, $values);
	}
	
	

	/**
	 * Création d'une instance, pour un manifest CTX
	 * 
	 * @param array $values Tableau associatif clef,valeur
	 * @return Manifest
	 */
	static function describe_ctx($values)
	{
		return self::describe(self::MANIFEST_CONTEXT, $values);
	}
	

	
	/**
	 * Création d'une instance, pour un manifest PAGE
	 * 
	 * @param array $values Tableau associatif clef,valeur
	 * @return Manifest
	 */
	static function describe_page($values)
	{
		return self::describe(self::MANIFEST_PAGE, $values);
	}
	
	
	
	/**
	 * Obtenir un manifest, depuis le cache ou le lire
	 *
	 * @param Name_space $ns Objet namespace courant
	 * @param string $mtype Type de manifest à lire (app, ctx, page)
	 * @return null|Manifest Renvoie le manifest demandé, ou null si inexistant
	 */
	static function getManifest(Name_space $ns, $mtype)
	{
		// si manifest déjà dans le cache
		if ( $m = self::_manifestFromCache($ns, $mtype) )
			return $m;
			
		// sinon, lire le manifest demandé en le créant
		if ( $m = self::_manifestFromFile($ns, $mtype) )
			return self::_manifestToCache($m, $ns);
		else
			return NULL;
	}
	
	// ---- DECL. STATIQUES ----]
	
	
	// [---- DECL. PUBLIQUES ----

	// type du manifest et namespace associé	
	public $kind = NULL;
	//public $ns = NULL;

	
	/**
	 * Accesseur magique pour lire une valeur associée à une clef dans la propriété $this->_values
	 * 
	 * @param string $k
	 * @return mixed
	 */
	public function __get($k)
	{
		if ( array_key_exists($k, $this->_values) )
			return $this->_values[$k];
		else
			return null;
	}
	
	
	
	/**
	 * Accesseur magique pour définir une valeur associée à une clef dans la propriété $this->_values
	 * 
	 * @param string $k
	 * @param mixed $v
	 */
	public function __set($k, $v) { $this->_values[$k] = $v; }
	
	// ---- DECL. PUBLIQUES ----]
}

?>