<?php

namespace Ppast\App_Framework;



// proposer une commande par défaut pour une commande inexistante ; celle-ci renvoie systématiquement un échec
class DefaultNoCommand extends Command
{
	/**
	 * Méthode contenant le code réel de la commande
	 *
	 * Pour cette commande par défaut pour une commande inexistante, renvoyer un statut d'erreur CMD_NOT_FOUND
	 *
	 * @param Request $req
	 * @return int Renvoie un code de statut
	 */
	function doExecute(Request $req)
	{
		return self::CMD_NOT_FOUND;
	}
}


?>