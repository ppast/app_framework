<?php

namespace Ppast\App_Framework\RegistryProviders;



// classe pour gérer l'accès aux paramètres d'application stockés dans mysql
class DbApplication extends Provider
{
	// --- DECL. PROTEGEES  ---
	protected $_db;

	// statements en cache
	protected $_pdo_st_get = NULL;
	protected $_pdo_st_set = NULL;
	protected $_pdo_st_test = NULL;
	protected $_kvalue = NULL;
	protected $_tablename = NULL;
	protected $_kgroupe = NULL;
	protected $_kid = NULL;
	
	
	/**
	 * Obtenir un PdoStatement déjà créé, ou le créer, pour l'obtention d'une valeur
	 *
	 * @return \PdoStatement
	 */
	protected function _getStatement_get()
	{
		if ( !$this->_pdo_st_get )
			$this->_pdo_st_get = $this->_db->prepare("SELECT {$this->_kvalue} FROM {$this->_tablename} WHERE {$this->_kgroupe}=:ctx AND {$this->_kid}=:k LIMIT 1");
			
		return $this->_pdo_st_get;
	}
	

	
	/**
	 * Obtenir un PdoStatement déjà créé, ou le créer, pour tester l'existence d'une clef
	 *
	 * @return \PdoStatement
	 */
	protected function _getStatement_test()
	{
		if ( !$this->_pdo_st_test )
			$this->_pdo_st_test = $this->_db->prepare("SELECT COUNT({$this->_kid}) FROM {$this->_tablename} WHERE {$this->_kgroupe}=:ctx AND {$this->_kid}=:k LIMIT 1");
			
		return $this->_pdo_st_test;
	}
	

	
	/**
	 * Obtenir un PdoStatement déjà créé, ou le créer, pour la définition d'une valeur
	 *
	 * @return \PdoStatement
	 */
	protected function _getStatement_set()
	{
		if ( !$this->_pdo_st_set )
			$this->_pdo_st_set = $this->_db->prepare("UPDATE {$this->_tablename} SET {$this->_kvalue}=:v WHERE {$this->_kid}=:k AND {$this->_kgroupe}=:ctx LIMIT 1");
			
		return $this->_pdo_st_set;
	}
	
	// --- /DECL. PROTEGEES ---
	
	
	
	/**
	 * Constructeur 
	 *
	 * @param \Pdo $db
	 * @param string $kvalue Nom de la colonne de valeur dans la table
	 * @param string $tablename Nom de la table des paramètres
	 * @param string $kgroupe Nom du contexte dans la table
	 * @param string $kid Nom de la colonne clef primaire dans la table
	 */
	public function __construct(\Pdo $db, $kvalue = 'valeur', $tablename = 'Params', $kgroupe = 'groupe', $kid = 'id')
	{
		$this->_db = $db;
		$this->_kvalue = $kvalue;
		$this->_tablename = $tablename;
		$this->_kgroupe = $kgroupe;
		$this->_kid = $kid;
	}
	
	
	
	/**
	 * Obtenir base de données liée à la stratégie
	 *
	 * @return \Pdo
	 */
	public function db()
	{
		return $this->_db;
	}
	
	
	
	/**
	 * Accesseur lecture pour la stratégie implémentant le registre
	 *
	 * @param string $k Clef
	 * @param string $ctx Contexte
	 * @param mixed $def Valeur par défaut
	 */
	public function get($k, $ctx, $def = NULL)
	{
		// si tableau groupe/clef
		if ( !empty($ctx) && !empty($k) )
		{
			$st = $this->_getStatement_get();
			$st->bindValue(':ctx', $ctx); 
			$st->bindValue(':k', $k);

			if ( $st->execute() )
				if ( $row = $st->fetch(\PDO::FETCH_NUM) )
					return $row[0];
				else
					return $def;
			else
				return $def;
							
			$st->closeCursor();
		}
		else
			return $def;
	}
	
	
	
	/**
	 * Tester l'existence d'une clef (pas que la valeur associée existe)
	 *
	 * @param string $k Clef
	 * @param string $ctx Contexte
	 * @return bool
	 */
	public function test($k, $ctx)
	{
		if ( !empty($ctx) && !empty($k) )
		{
			$st = $this->_getStatement_test();
			$st->bindValue(':ctx', $ctx); 
			$st->bindValue(':k', $k);

			// par construction, la requête renverra toujours une ligne, puisque on a fait un SELECT COUNT(id)
			if ( $st->execute() )
				return $st->fetchColumn(0) > 0;
			else
				return false;
							
			$st->closeCursor();
		}
		else
			return false;
	}
	

	
	/**
	 * Accesseur écriture pour la stratégie implémentant le registre
	 *
	 * @param string $k Clef
	 * @param string $ctx Contexte
	 * @param mixed $v Valeur
	 */
	public function set($k, $ctx, $v)
	{
		// si tableau groupe/clef
		if ( !empty($ctx) && !empty($k) )
		{
			$st = $this->_getStatement_set();
			$st->bindValue(':ctx', $ctx); 
			$st->bindValue(':k', $k);
			$st->bindValue(':v', $v);
			$st->execute();
			$st->closeCursor();
		}

		return $v;
	}
}


?>