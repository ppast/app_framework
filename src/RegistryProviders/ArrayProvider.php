<?php

namespace Ppast\App_Framework\RegistryProviders;



// classe pour gérer l'accès aux paramètres d'application stockés dans un tableau volatile
class ArrayProvider extends Provider
{
	// --- DECL. PRIVEES ---
	private $_items;
	// --- /DECL. PRIVEES ---

	
	/**
	 * Constructeur 
	 */
	function __construct()
	{
		$this->_items = [];
	}
	
	
	
	/**
	 * Accesseur lecture pour la stratégie implémentant le registre
	 *
	 * @param string $k Clef
	 * @param string $ctx Contexte
	 * @param mixed $def Valeur par défaut
	 */
	function get($k, $ctx, $def = NULL)
	{
		if ( $this->test($k, $ctx) )
			return $this->_items[$ctx . "__" . $k];
		else
			return $def;
	}
	
	
	
	/**
	 * Accesseur écriture pour la stratégie implémentant le registre
	 *
	 * @param string $k Clef
	 * @param string $ctx Contexte
	 * @param mixed $v Valeur
	 */
	function set($k, $ctx, $v)
	{
		return $this->_items[$ctx . "__" . $k] = $v;
	}
	
	
	
	
	/**
	 * Tester l'existence d'une clef (pas que la valeur associée existe)
	 *
	 * @param string $k Clef
	 * @param string $ctx Contexte
	 * @return bool
	 */
	function test($k, $ctx)
	{
		return array_key_exists($ctx . "__" . $k, $this->_items);
	}
}


?>