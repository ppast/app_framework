<?php

namespace Ppast\App_Framework\RegistryProviders;



// classe pour gérer l'accès concrêt au registre (stratégie) où on force le contexte
class EnsureContext extends Provider
{
	// --- DECL. PRIVEES ---

	protected $_reg;

	// --- /DECL. PRIVEES ---

	/** 
	 * Constructeur
	 * 
	 * @param \Ppast\App_Framework\Registries\Registry $reg Registre dont il faut forcer le contexte
	 */
	public function __construct(\Ppast\App_Framework\Registries\Registry $reg)
	{
		$this->_reg = $reg;
	}
	
	
	
	/**
	 * Accesseur lecture pour la stratégie implémentant le registre
	 *
	 * @param string $k Clef
	 * @param string $ctx Contexte
	 * @param mixed $def Valeur par défaut
	 */
	public function get($k, $ctx, $def = NULL)
	{
		return $this->_reg->get($k, $ctx, $def);
	}
	
	
	
	/**
	 * Accesseur écriture pour la stratégie implémentant le registre
	 *
	 * @param string $k Clef
	 * @param string $ctx Contexte
	 * @param mixed $v Valeur
	 */
	public function set($k, $ctx, $v)
	{
		return $this->_reg->set($k, $ctx, $v);
	}
	
	
	
	/**
	 * Tester l'existence d'une clef (pas que la valeur associée existe)
	 *
	 * @param string $k Clef
	 * @param string $ctx Contexte
	 * @return bool
	 */
	function test($k, $ctx)
	{
		return $this->_reg->test($k, $ctx);
	}
}



?>