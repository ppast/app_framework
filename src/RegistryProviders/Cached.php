<?php

namespace Ppast\App_Framework\RegistryProviders;



// classe pour gérer l'accès aux paramètres avec gestion d'un cache
class Cached extends Provider
{
	// --- DECL. PRIVEES ---

	protected $_reg = NULL;
	protected $_cache = array();

	// --- /DECL. PRIVEES ---

	/**
	 * Constructeur
	 *
	 * @param \Ppast\App_Framework\Registries\Registry $reg Registre à mettre en cache
	 */
	public function __construct(\Ppast\App_Framework\Registries\Registry $reg)
	{
		$this->_reg = $reg;
	}
	
	
	
	/**
	 * Accesseur lecture pour la stratégie implémentant le registre
	 *
	 * @param string $k Clef
	 * @param string $ctx Contexte
	 * @param mixed $def Valeur par défaut
	 */
	function get($k, $ctx, $def = NULL)
	{
		// si absent du cache, le chercher dans le registre sous-jacent, et stocker la valeur
		if ( !$this->testCache($k, $ctx) )
			$this->_cache[$ctx . '__' . $k] = $this->_reg->get($k, $ctx, $def);
		
		return $this->_cache[$ctx . '__' . $k];
	}
	
	
	
	/**
	 * Accesseur écriture pour la stratégie implémentant le registre
	 *
	 * @param string $k Clef
	 * @param string $ctx Contexte
	 * @param mixed $v Valeur
	 */
	function set($k, $ctx, $v)
	{
		// modifier dans cache et dans registre sous-jacent
		$this->_cache[$ctx . "__" . $k] = $v;
		$this->_reg->set($k, $ctx, $v);
		
		return $v;
	}
	
	
	
	/**
	 * Tester l'existence d'une clef (pas que la valeur associée existe) dans le cache
	 *
	 * @param string $k Clef
	 * @param string $ctx Contexte
	 * @return bool
	 */
	function testCache($k, $ctx)
	{
		return array_key_exists($ctx . "__" . $k, $this->_cache);
	}
	
	
	
	/**
	 * Tester l'existence d'une clef (pas que la valeur associée existe) dans le cache ; si pas présente, rechercher dans le registre sous-jacent
	 *
	 * @param string $k Clef
	 * @param string $ctx Contexte
	 * @return bool
	 */
	function test($k, $ctx)
	{
		if ( !$this->testCache($k, $ctx) )
			return $this->_cache->test($k, $ctx);
		else
			return true;
	}
}


?>