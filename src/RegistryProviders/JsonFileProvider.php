<?php

namespace Ppast\App_Framework\RegistryProviders;



// classe pour gérer l'accès aux paramètres d'application stockés dans un fichier json
class JsonFileProvider extends Provider
{
	// --- DECL. PRIVEES ---
	protected $_fname;
    protected $_json;
	// --- /DECL. PRIVEES ---

	
	/**
	 * Constructeur 
	 * 
	 * @param string $fname Fichier json
	 */
	function __construct($fname)
	{
		$this->_fname = $fname;
        
        if ( !file_exists($fname) )
            $this->_json = (object)[];
        else
        {
            $this->_json = json_decode(file_get_contents($fname));
            if ( is_null($this->_json) )
                throw new \Exception("Erreur lecture configuration JSON");
        }
	}
	
	
	
	/**
	 * Accesseur lecture pour la stratégie implémentant le registre
	 *
	 * @param string $k Clef
	 * @param string $ctx Contexte
	 * @param mixed $def Valeur par défaut
	 */
	function get($k, $ctx, $def = NULL)
	{
		if ( $this->test($k, $ctx) )
			return $this->_json->$ctx->$k;
		else
			return $def;
	}
	
	
	
	/**
	 * Accesseur écriture pour la stratégie implémentant le registre
	 *
	 * @param string $k Clef
	 * @param string $ctx Contexte
	 * @param mixed $v Valeur
	 */
	function set($k, $ctx, $v)
	{
		// créer le contexte le cas échéant
		if ( !property_exists($this->_json, $ctx) )
			$this->_json->$ctx = (object)[];
            
		$this->_json->$ctx->$k = $v;
        
        
        $f = fopen($this->_fname, 'w');
        fwrite($f, json_encode($this->_json, JSON_PRETTY_PRINT));
        fclose($f);
	}
	
	
	
	/**
	 * Tester l'existence d'une clef (pas que la valeur associée existe)
	 *
	 * @param string $k Clef
	 * @param string $ctx Contexte
	 * @return bool
	 */
	function test($k, $ctx)
	{
		if ( is_null($this->_json->$ctx) )
			return false;
		else
			return property_exists($this->_json->$ctx, $k);
	}
}

?>