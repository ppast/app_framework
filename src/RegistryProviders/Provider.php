<?php

namespace Ppast\App_Framework\RegistryProviders;



// classe abstraite pour gérer l'accès concrêt au registre (stratégie)
abstract class Provider
{
	/**
	 * Accesseur lecture pour la stratégie implémentant le registre
	 *
	 * @param string $k Clef
	 * @param string $ctx Contexte
	 * @param mixed $def Valeur par défaut
	 */
	abstract function get($k, $ctx, $def = NULL);

	
	
	/**
	 * Accesseur écriture pour la stratégie implémentant le registre
	 *
	 * @param string $k Clef
	 * @param string $ctx Contexte
	 * @param mixed $v Valeur
	 */
	abstract function set($k, $ctx, $v);

	
	
	/**
	 * Tester l'existence d'une clef (pas que la valeur associée existe)
	 *
	 * @param string $k Clef
	 * @param string $ctx Contexte
	 * @return bool
	 */
	abstract function test($k, $ctx);
}


?>