<?php

namespace Ppast\App_Framework;



// commande de base pour xmlhttp : on modifie le gestionnaire de rendu par défaut
abstract class XMLHttpCommand extends Command
{
	/**
	 * Constructeur
	 */
	function __construct()
	{
		parent::__construct(StdoutController::OUTPUT_XMLHTTP);
	}
}


?>