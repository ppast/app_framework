<?php

namespace Ppast\App_Framework;



// commande de base pour download : on modifie le gestionnaire de rendu par défaut
abstract class DownloadCommand extends Command
{
	/**
	 * Constructeur
	 */
	function __construct()
	{
		parent::__construct(StdoutController::OUTPUT_DOWNLOAD);
	}
	
	
	/**
	 * Définir le téléchargement d'un fichier
	 * 
	 * @param Request $req Requête en cours
	 * @param string $contentType Valeur ContentType du contenu qui sera renvoyé au navigateur
	 * @param string $filename Suggestion de nom de fichier utilisé pour le téléchargement
	 * @param string $file Fichier dont il faut renvoyer le contenu
	 * @param bool $delete Indiquer True si on veut effacer le fichier $file à l'issue de l'envoi au navigateur
	 * @return Request Renvoie $req
	 */
	function setFileDownload(Request $req, $contentType, $filename, $file, $delete = false)
	{
		return $this->getOutputHandler()->setFileDownload($req, $contentType, $filename, $file, $delete);
	}
	
	
	
	/**
	 * Définir le téléchargement de données brutes
	 * 
	 * @param Request $req Requête en cours
	 * @param string $contentType Valeur ContentType du contenu qui sera renvoyé au navigateur
	 * @param string $filename Suggestion de nom de fichier utilisé pour le téléchargement
	 * @param string $output Contenu à renvoyer
	 * @return Request Renvoie $req
	 */
	function setDownload(Request $req, $contentType, $filename, $output)
	{
		return $this->getOutputHandler()->setDownload($req, $contentType, $filename, $output);
	}
}


?>