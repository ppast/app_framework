<?php

namespace Ppast\App_Framework\Bootstrap\ExceptionHandlers;


use \Nettools\Core\ExceptionHandlers\SimpleExceptionHandler;




// classe pour traiter la sécurité du script
class ExceptionHandler extends SimpleExceptionHandler
{
    /**
	 * Traiter une exception pendant le rendu PageContainer et écrire dans la sortie standard
	 *
	 * @param \Throwable $e
	 */
    protected function _handlePageContainerException(\Throwable $e)
    {
		// obtenir exception
		$ex = $this->_getException($e, 'Erreur pendant l\'affichage de la page');
		
        // fermer d'éventuelles balises ouvertes pour que la position d'affichage de l'erreur soit meilleure
		echo '</table></ul></option></select>';

        // ecriture reset css ; les font-size sont en em, s'adaptant aux différentes tailles d'affichage
		echo <<<RAZCSS
		
<style>

.raz_css_exception{
	font-size:16px;
}


/* version mobile */
@media screen and (max-width:800px) {
	.raz_css_exception{
		font-size:9.6px;
	}
}


/* version >1400 : grossir les caractères */
@media screen and (min-width:1400px) {
	.raz_css_exception {
		font-size:17.6px;
	}
}

</style>
		
RAZCSS;
		
		echo '<div class="raz_css_exception">';
		
		// ecriture exception formatée, qui prend en charge version mobile, normale, grand écran (pour font-size)
		echo $ex;
		
		// écriture lien mobile, pas possible de le masquer car on est sur exception dans pagerenderer, ce qui peut
		// rendre difficile la lisibilité
		echo self::_mobileLink($ex, false);
		
		echo '</div>';
    }
    
    
	
	/**
	 * Afficher une représentation lisible d'une exception en HTML et arrêter le script
	 *
	 * @param \Throwable $e
	 */
	public function handleException(\Throwable $e)
	{
		// si pendant pagecontainer, faire un reset CSS, adapter taille caractères et imposer affichage lien mobile
		if ( is_int(strpos($e->getTraceAsString(), 'Ppast\\App_Framework\\Bootstrap\\PageRenderer::doTemplate')) )
        	$this->_handlePageContainerException($e);
        else
            parent::handleException($e);

        die();
    }   
    
	// --- /DECL. STATIQUES ---
}

?>