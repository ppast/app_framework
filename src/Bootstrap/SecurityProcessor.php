<?php

namespace Ppast\App_Framework\Bootstrap;


use \Ppast\App_Framework\Name_space;
use \Ppast\App_Framework\Manifest;
use \Ppast\App_Framework\Bootstrap;



// classe pour traiter la sécurité du script
 class SecurityProcessor
{
	// --- DECL. PRIVEES ---
	private function __construct() {} // pas d'instanciation publique possible, classe uniquement statique

	

	// cache gestionnaires de sécurité
	private static $_securityHandlers = NULL;
	
    // --- /DECL. PRIVEES ---



    
	// --- DECL. STATIQUES ---
	
    
	/**
	 * Obtenir un gestionnaire de sécurité nommé
	 *
	 * @param string $cname Nom de classe du gestionnaire, sans namespace
	 * @return null|\Ppast\App_Framework\Bootstrap\SecurityHandlers\Handler Renvoie le gestionnaire demandé, ou null si inexistant
	 * @throws \Ppast\App_Framework\Bootstrap\Exceptions\Security Exception levée si l'initialisation des gestionnaires de sécurité n'a pas encore eu lieu
	 */
	public static function getSecurityHandler($cname)
	{
 		// si gestionnaires non initialisés, renvoyer false
		if ( !self::$_securityHandlers )
			throw new Exceptions\Security('Les gestionnaires de sécurité n\'ont pas encore été initialisés');
			
			
		// parcourir la liste des gestionnaires jusqu'à trouver celui voulu
		return self::$_securityHandlers[$cname];
    }
    
    
    
	/**
	 * Obtenir les gestionnaires de sécurité pour le namespace courant ; créer et mettre en cache le cas échéant
	 *
	 * @param \Ppast\App_Framework\Name_space $ns
	 * @return \Ppast\App_Framework\Bootstrap\SecurityHandlers\Handler[] Renvoie un tableau associatif (clef,valeur) où clef est le nom de classe du gestionnaire et valeur est une instance de ce gestionnaire
	 * @throws \Ppast\App_Framework\Bootstrap\Exceptions\NotFound Exception levée si un gestionnaire de sécurité n'est pas trouvé
	 */
	public static function getSecurityHandlers(Name_space $ns)
	{
		// si cache inexistant
		if ( is_null(self::$_securityHandlers) )
		{
			// prendre les manifests dans cet ordre, pour raffiner au fur et à mesure jusqu'à la page
			$kinds = array(Manifest::MANIFEST_APP, Manifest::MANIFEST_CONTEXT, Manifest::MANIFEST_PAGE);
			self::$_securityHandlers = array();			
	
			
			foreach ( $kinds as $kind )
			{
				// includes au niveau de l'appli, contexte ou page
				$m = Manifest::getManifest($ns, $kind);
				if ( $m && ($shs = $m->{Manifest::SECURITY_HANDLERS}) )
					foreach ( $shs as $sh )
					{
						// si ligne dans tableau des gestionnaires est une chaine, c'est un nom de classe, sans namespace
						if ( is_string($sh) )
						{
							$cname = $sh;
							$args = [array()];
						}
						// si ligne est un tableau, alors le premier élément est le nom de classe (sans namespace), le reste contient les paramètres du constructeur, dans le même ordre
						elseif ( is_array($sh) )
						{
							$cname = $sh[0];
							$args = array_slice($sh, 1);
						}

												
						try
						{
							$c = new \ReflectionClass("\\Ppast\\App_Framework\\Bootstrap\\SecurityHandlers\\$cname");
							self::$_securityHandlers[$cname] = $c->newInstanceArgs($args);
						}
						catch ( \ReflectionException $e )
						{
							throw new \Ppast\App_Framework\Bootstrap\Exceptions\NotFound("Le gestionnaire de sécurité '$cname' n'existe pas ou ne peut être créé");
						}							
					}
			}
		}

		
		return self::$_securityHandlers;
	}

    
    
	/**
	 * Vérifier que la session est toujours présente
	 *
	 * @param bool|string $login Contient false en utilisation normale, ou 'login' ou 'logout' pour les cas particuliers d'utilisation
	 * @throws \Ppast\App_Framework\Bootstrap\Exceptions\NotFound Exception levée si la session n'est pas trouvée
	 */
	public static function checkSession($login)
	{
		switch ( $login )
		{
			case false:
				if ( Bootstrap::$session->get(__CLASS__) != __CLASS__ )
					throw new \Ppast\App_Framework\Bootstrap\Exceptions\NotFound("La session est corrompue : ré-identification nécessaire");
                
                break;
				
			case 'login': 
				// définir un marqueur dans la session ; quand on voudra la réouvrir, si marqueur absent, cela signifie que le serveur a perdu la session
				Bootstrap::$session->set(__CLASS__, __CLASS__);
				break;
				
			case 'logout':
				Bootstrap::$session->delete(__CLASS__);
				break;
		}
	}
	
    
	
    /**
	 * Sécuriser l'application
	 *
	 * @param \Ppast\App_Framework\Bootstrap\SecurityHandlers\Composite $csh Gestionnaire de sécurité composite qui valide l'ensemble des gestionnaires qu'il regroupe
	 * @param string $login Contient false dans le cas général, ou pour les cas particuliers, contient 'login' ou 'logout'
	 * @param array $logindata Informations de login en tableau associatif
	 * @throws \Ppast\App_Framework\Bootstrap\Exceptions\Auth Exception levée si l'autorisation n'est pas valide
	 * @throws \Ppast\App_Framework\Bootstrap\Exceptions\NotFound Exception levée si un gestionnaire de sécurité n'a pas les paramètres requis
	 */
	public static function doSecurity(SecurityHandlers\Composite $csh, $login, array $logindata)
	{
		// vérifier session
		self::checkSession($login);			
		
		
        // tester présence des paramètres nécessaires au fonctionnement des gestionnaires de sécurité ; exception si problème
		$csh->testParameters();

        
		// si login/logout, initialiser ou détruire la session ; sinon passer à l'authentification
		switch ( $login )
		{
			case 'login' :
                // mémoriser identification user, par exemple ; la vérif que le password est bon n'est faite que dans authorize, au reboot !
				$csh->initialize(Bootstrap::$session, $logindata);
				return;

			case 'logout' :
				Bootstrap::$session->destroy();
				return;
		}

        
        // authentifier la connexion (c'est au reboot, ici, que le mot de passe est validé la première fois ; ensuite aussi bien sûr ; la 
        // différence est que la première fois, au reboot, l'erreur est renvoyée à l'expéditeur dans __error__ via une redirection)
		// exception si problème faite dans Bootstrap qui intercepte l'exception Auth
		$csh->authorize(Bootstrap::$session);
	}
	
	// --- /DECL. STATIQUES ---
}

?>