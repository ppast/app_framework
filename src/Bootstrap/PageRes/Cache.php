<?php

namespace Ppast\App_Framework\Bootstrap\PageRes;


use \Ppast\App_Framework\Name_space;
use \Ppast\App_Framework\Request;
use \Ppast\App_Framework\Manifest;
use \Ppast\App_Framework\Utils;
use \Ppast\App_Framework\Bootstrap;
use \Ppast\App_Framework\Application;



// classe pour cache ressources JS/CSS
class Cache
{
	public $path;
	
	
	/** 
	 * Créer un dossier de cache
	 */
	protected function _create()
	{
		// créer dossier cache et créer à l'intérieur un fichier htaccess qui compresse le cache et définit une politique de cache
		mkdir(Utils::documentRoot() . $this->path);
		$fhta = fopen(Utils::documentRoot() . $this->path . '.htaccess', 'w');
		fwrite ($fhta, 	"# GESTION DU CACHE\n" .
						"<IfModule mod_expires.c>\n" .
						"	<FilesMatch \"\\.(cache)\$\">\n" . 
						"		ExpiresActive On\n" .
						"		ExpiresDefault \"access plus 365 days\"\n" .
						"		ExpiresByType application/javascript \"access plus 365 days\"\n" .
						"		ExpiresByType text/css \"access plus 365 days\"\n" .
						"	</FilesMatch>\n" .
						"</IfModule>\n" .
						"\n" .
						"\n" .
						"\n" .
						"# Aucun script dans le dossier et ses sous-dossiers, que ce soit PHP, PERL ou autre CGI, ne pourra s'executer si ExecCGI est inactif. Et interdit d'afficher la liste des fichiers.\n" .
						"<Files ~ \"(\\.php[3-9]?)\$\">\n" .
						"	OPTIONS -ExecCGI\n" .
						"</Files>\n"						
						);
		fclose($fhta);
	}
	


	/**
	 * Constructeur cache
	 *
	 * @param \Ppast\App_Framework\Name_space $ns Namespace courant app/ctx/page
	 */
	public function __construct(Name_space $ns)
	{
		$this->path = Bootstrap::$approot . $ns->toAppPath() . '/_cache/'; // approot se termine par '/' ou est vide, et ne commence pas par /

		
		// tester existence dossier de cache, le créer si besoin
		if ( !file_exists(Utils::documentRoot() . $this->path) )
			$this->_create();		
	}
	
	
	
	/**
	 * Tester si le mécanisme de cache des ressources JS/CSS est activé 
	 *
	 */
	public static function isActive(Name_space $ns)
	{
		$mapp = Manifest::getManifest($ns, Manifest::MANIFEST_APP);
		return $mapp && $mapp->{Manifest::MINIFY};
	}
	
	
	

	/** 
	 * Créer une concaténation de resources CSS ou JS pour optimiser le chargement
	 * 
	 * @param HtmlReference[] $res Liste d'objets HtmlReference à fusionner
	 * @param \Ppast\App_Framework\Name_space $ns Namespace courant app/ctx/page
	 * @param string $kind Contient CSS ou JS
	 * @param HtmlReference[] Contiendra au retour un tableau d'objets HtmlReference qui n'ont pas pu être fusionnés
	 * @return null|HtmlReference Renvoie un objet HtmlReference pointant vers résultat de la fusion en cache ou NULL si la fusion est vide
	 */
	public function merge(array $res, Name_space $ns, $kind, array &$ignored)
	{
		// déterminer les inclusions qui peuvent être mise en cache (exclure les références externes)
		$tomerge = array();
		foreach ( $res as $f )
			// si référence externe, ne pas mettre en cache
			if ( $f->isLink() )
				$ignored[] = $f;
			else
				$tomerge[] = $f;
		

		// si aucun fichier à fusionner et mettre en cache (tous exclus !), sortir maintenant
		if ( !count($tomerge) )
			return NULL;
		


		// calculer un tableau avec les chemins des ressources et leurs numéros de version respectifs
		$tomerge_versionning = array();
		foreach ( $tomerge as $f )
			$tomerge_versionning[] = $f->getVersionId();
		

		// on a maintenant dans $tomerge_versionning une liste des chemins avec un numéro de version
		// calculer le hash de la liste ; avec ce versionning, toute nouvelle version engendrera un cache différent, et cela règle le pb de cache sur les postes clients
		$cache = $this->path . md5(implode('', $tomerge_versionning)) . "." . strtolower($kind) . ".cache";
		$cachepath = Utils::documentRoot() . $cache;


		// si ce fichier de cache n'existe pas encore, le créer
		if ( !file_exists($cachepath) )
		{
			$fi = fopen($cachepath, 'w');

			
			// pour toutes les références HtmlReference (CSSInclude ou JSInclude)
			foreach ( $tomerge as $f )
			{
				fwrite($fi, $f->getContent());
				fwrite($fi, "\n\n");
			}

			fclose($fi);
		}

		
		
		// quand on arrive ici, on a éventuellement des resources dans $ignored, qui seront renvoyées (argument passé par référence &)
		// retourner un nouvel objet CSSInclude ou JSInclude avec le chemin vers le fichier fusionné en cache (chemin absolu depuis racine web)
		return (new \ReflectionClass('\\Ppast\\App_Framework\\Bootstrap\\PageRes\\' . $kind . 'Include'))->newInstance("/$cache", $ns);
	}
	
	
	
}

?>