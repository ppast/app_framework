<?php

namespace Ppast\App_Framework\Bootstrap\PageRes;


use \Ppast\App_Framework\Name_space;
use \Ppast\App_Framework\Request;
use \Ppast\App_Framework\Manifest;
use \Ppast\App_Framework\Utils;
use \Ppast\App_Framework\Bootstrap;
use \Ppast\App_Framework\Application;



// classe de ressource référencées dans un include (php, js, css)
class Reference
{
	public $url;
	public $ns;
	
	
	
	/**
	 * Obtenir la liste des placeholders possibles depuis le manifest app
	 *
	 * @param \Ppast\App_Framework\Name_space $ns Namespace courant app/ctx/page
	 * @return string[] Renvoie une liste des placeholders possibles
	 */
	protected static function _getPlaceholdersEnum(Name_space $ns)
	{
		$m = Manifest::getManifest($ns, Manifest::MANIFEST_APP);
		if ( !$m )
			return [];
		
		// on tente de remplacer dans les valeurs des paramètres de configuration des variables %xxxx% où xxxx désigne une autre valeur de configuration
		// on peut ainsi fabriquer des jeux de paramètres composites tels que :
		// root = /path/to/root
		// app = %root%/
		$cfg = $m->{Manifest::CONFIG};
		return $cfg ? array_keys($cfg) : [];
	}

	
	
	/**
	 * Traiter les placeholders %xxxx% dans une chaîne
	 *
	 * @param string $url Reference chaîne à traiter
	 * @param string[] $placeHoldersEnum Liste des placeHolders à rechercher dans $url
	 * @return string Renvoie $url dans laquelle les placeholders ont été remplacés par leurs valeurs respectives
	 */
	protected static function _handlePlaceholders($url, array $placeholdersEnum)
	{
		if ( !count($placeholdersEnum) )
			return $url;
		
		
		$array_search = array();
		$array_replace = array();
		foreach ( $placeholdersEnum as $var )
		{
			$array_search[] = "%$var%";
			$array_replace[] = Bootstrap::config($var, '', false);
		}


		// on a maintenant un tableau %vendor_dir%, %browser_tools_dir% avec les valeurs de remplacement correspondantes
		$count = 0;
		do
		{
			// opérer les remplacements, éventuellement récursivement, jusqu'à terminé
			$url = str_replace($array_search, $array_replace, $url, $count);
		}
		while ( $count );
		
		
		// suite aux remplacements, il se peut qu'on trouve des // dans le chemin, remplacer par un seul /
		return str_replace(['http:$$', 'https:$$'], ['http://', 'https://'], str_replace('//', '/', str_replace(['http://', 'https://'], ['http:$$', 'https:$$'], $url)));
	}
	
	
	
	/**
	 * Constructeur de la resource include
	 *
	 * @param string $url
	 * @param \Ppast\App_Framework\Name_space $ns Namespace courant app/ctx/page
	 */
	public function __construct($url, Name_space $ns)
	{
		$this->url = self::_handlePlaceholders($url, self::_getPlaceholdersEnum($ns));
		$this->ns = $ns;
	}
	
	
	
	/**
	 * Tester si la resource est externe (http)	
	 * 
	 * @return bool
	 */
	public function isLink()
	{
		return preg_match('~^http(s)?://~', $this->url);
	}
	
	
	
	/**
	 * Construire le chemin vers la resource désignée par $url
	 *
	 * Le chemin peut être :
	 * - externe (http://xxxx)
	 * - absolu depuis racine web (/ab/cd/ef.php)
	 * - relatif (file.php) à la décomposition courante app/ctx/page, depuis racine application (approot)
	 * - relatif (ab/cd/gh.php) à la racine de l'application (approot)
	 *
	 * @return string Renvoie le chemin complet, éventuellement calculé
	 */
	public function buildPath()
	{
		// si chemin externe, renvoyer tel quel $this->url
		if ( $this->isLink() )
			return $this->url;
		
		
		// si chemin absolu depuis racine web (/ab/cd/ef.php), renvoyer tel quel
		if ( (substr($this->url, 0, 1) == '/') )
			return $this->url;
		
		
		// si chemin (ab/cd/gh.php) relatif à la racine de l'application (approot), on a un '/' mais pas au début (cas précédent)
		if ( is_int(strpos($this->url, '/')) )
			return '/' . Bootstrap::$approot . $this->url;		// rajouter au début de l'url la racine de l'application
		
		
		// sinon, le chemin est relatif à la décomposition app/ctx/page courante, préfixée par la racine application
		if ( file_exists(Utils::documentRoot() . Bootstrap::$approot . $this->ns->toPage() . '/' . $this->url) )
			return '/' . Bootstrap::$approot . $this->ns->toPage() . '/' . $this->url;
		else if ( file_exists(Utils::documentRoot() . Bootstrap::$approot . $this->ns->toCtxPath() . '/' . $this->url) )
			return '/' . Bootstrap::$approot . $this->ns->toCtxPath() . '/' . $this->url;
		else
			return '/' . Bootstrap::$approot . $this->ns->toAppPath() . '/' . $this->url;
	}
}

?>