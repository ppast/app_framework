<?php

namespace Ppast\App_Framework\Bootstrap\PageRes;


use \Ppast\App_Framework\Name_space;
use \Ppast\App_Framework\Request;
use \Ppast\App_Framework\Manifest;
use \Ppast\App_Framework\Utils;
use \Ppast\App_Framework\Bootstrap;
use \Ppast\App_Framework\Application;



// classe pour include PHP
class PHPInclude extends Reference
{
	/**
	 * Inclure le fichier PHP référencé
	 *
	 * @throws \Ppast\App_Framework\Bootstrap\Exceptions\NotFound Exception levée si la ressource référencée n'existe pas 
	 */
	public function doInclude()
	{
		// calculer le chemin réel
		$bpath = $this->buildPath();
		$path = Utils::documentRoot() . ltrim($bpath, '/');
		
		
		// tester existence fichier
		if ( !file_exists($path) )
			throw new \Ppast\App_Framework\Bootstrap\Exceptions\NotFound("Impossible d'inclure '$bpath'");

		
		include_once $path;
	}
}

?>