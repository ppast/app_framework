<?php

namespace Ppast\App_Framework\Bootstrap\PageRes;


use \Ppast\App_Framework\Name_space;
use \Ppast\App_Framework\Request;
use \Ppast\App_Framework\Manifest;
use \Ppast\App_Framework\Utils;
use \Ppast\App_Framework\Bootstrap;
use \Ppast\App_Framework\Application;



// classe de base pour include CSS ou JS
abstract class HtmlReference extends Reference
{
	/**
	 * Obtenir code html d'inclusion du fichier référencé
	 *
	 * @return string Renvoie une définition HTML d'inclusion de la resource
	 */
	abstract function getHtml();
	
	
	
	/** 
	 * Obtenir un numéro de version pour cette resource
	 *
	 * @return string
	 */
	public function getVersionId()
	{
		$path = ltrim($this->buildPath(), '/');
		return $path . filemtime(Utils::documentRoot() . $path);
	}
	
	
	
	/**
	 * La ressource a-t-elle une version minifiée ?
	 *
	 * @return bool
	 */
	public function isMinified()
	{
		$path = Utils::documentRoot() . ltrim($this->buildPath(), '/');
		return file_exists(str_replace([".js", ".css"], [".min.js", ".min.css"], $path));
	}
	
	
	
	/**
	 * La ressource a-t-elle une version minifiée avec ancienne convention js|css.min ?
	 *
	 * @return bool
	 */
	public function isOldMinified()
	{
		$path = Utils::documentRoot() . ltrim($this->buildPath(), '/');
		return file_exists("$path.min");
	}
	
	
	
	/**
	 * Obtenir le contenu de la référence, de préférence en version minifiée
	 *
	 * @return string
	 */
	public function getContent()
	{
		$path = Utils::documentRoot() . ltrim($this->buildPath(), '/');
		return $this->isMinified() ? file_get_contents(str_replace([".js", ".css"], [".min.js", ".min.css"], $path)) : ($this->isOldMinified() ? file_get_contents("$path.min") : file_get_contents($path));
	}
}

?>