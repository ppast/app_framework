<?php

namespace Ppast\App_Framework\Bootstrap\PageRes;


use \Ppast\App_Framework\Name_space;
use \Ppast\App_Framework\Request;
use \Ppast\App_Framework\Manifest;
use \Ppast\App_Framework\Utils;
use \Ppast\App_Framework\Bootstrap;
use \Ppast\App_Framework\Application;



// classe de ressource pour une modèle de page
class Template
{
	const TITLE_TAG = 'COM_APP_FRMK_TITLE';
	const HEAD_TAG = 'COM_APP_FRMK_HEAD';
	const HEAD_TAG_CSS = 'COM_APP_FRMK_HEAD_CSS';
	const HEAD_TAG_JS = 'COM_APP_FRMK_HEAD_JS';
	const BODY_TAG = 'COM_APP_FRMK_BODY';
	const DEFAULT_VALUE = "<html><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\"><title><!--COM_APP_FRMK_TITLE--></title><!--COM_APP_FRMK_HEAD--></head><body><!--COM_APP_FRMK_BODY--></body></html>";
	
	
	public $value;
	
	
	/**
	 * Renvoyer la définition html du tag : <!--TAG-->
	 * 
	 * @param string $tagname Nom du tag
	 * @return string 
	 */
	protected static function _htmlTag($tagname)
	{
		return "<!--$tagname-->";
	}
	
	
	
	/**
	 * Normaliser le modèle communiqué en chaîne
	 *
	 * On traite notamment le repère HEAD_TAG pour le remplacer par CSS, JS
	 *
	 * @param string $template
	 * @return string
	 */
	protected static function _normalize($template)
	{
		return str_replace(
							// rechercher le tag HEAD
							self::_htmlTag(self::HEAD_TAG), 
						  	
							// et le remplacer par plusieurs sous-tags
							implode("\n", [
									self::_htmlTag(self::HEAD_TAG_CSS),
									self::_htmlTag(self::HEAD_TAG_JS)
								]),
			
							// dans
							$template
			);
	}
	
	
	
	/**
	 * Constructeur
	 * 
	 * @param string $url Chemin vers le template ; si non défini, un template par défaut sera utilisé
	 * @throws \Ppast\App_Framework\Bootstrap\Exceptions\NotFound Exception levée si la ressource référencée n'existe pas 
	 */
	public function __construct($url)
	{
		// si modèle vide, il n'a pas pu être trouvé dans les manifest ; en proposer un simple
		if ( !$url )
			$this->value = self::DEFAULT_VALUE;
		else
		{
			$f = Utils::documentRoot() . Bootstrap::$approot . $url;
			
			// tenter la lecture du modèle
			if ( file_exists($f) )
				$this->value = file_get_contents($f);
			else
				throw new \Ppast\App_Framework\Bootstrap\Exceptions\NotFound("Impossible de lire le modèle '$url'");
		}
		
		
		
		// normaliser le modèle
		$this->value = self::_normalize($this->value);
	}
	
	
	
	/**
	 * Rechercher une balise et la remplacer par une valeur
	 * 
	 * @param string $tag
	 * @param string $value
	 * @return Template Renvoie une référence à $this, pour chainage
	 */
	protected function replaceTag($tag, $value)		
	{
		$this->value = str_replace(self::_htmlTag($tag), $value, $this->value);
		return $this;
	}
	
	
	
	/**
	 * Définir les inclusions JS
	 *
	 * @param string $value Code HTML d'inclusion JS
	 * @return Template Renvoie une référence à $this, pour chainage
	 */
	protected function setJSIncludes($value)
	{
		return $this->replaceTag(self::HEAD_TAG_JS, $value);
	}
	
	
	
	/**
	 * Définir les inclusions CSS
	 *
	 * @param string $value Code HTML d'inclusion CSS
	 * @return Template Renvoie une référence à $this, pour chainage
	 */
	protected function setCSSIncludes($value)
	{
		return $this->replaceTag(self::HEAD_TAG_CSS, $value);
	}
	
	
	
	/** 
	 * Définir le titre
	 *
	 * @param string $title
	 * @return Template Renvoie une référence à $this, pour chainage
	 */
	protected function setTitle($title)
	{
		return $this->replaceTag(self::TITLE_TAG, $title);
	}
	
	
	
	/**
	 * Tester la présence d'une directive pour une inclusion conditionnelle
	 *
	 * @param string $incl Chemin d'inclusion à tester
	 * @return bool Renvoie true si l'inclusion peut avoir lieu (directive satisfaite) ou false si la directive n'est pas satisfaite
	 */
	protected function conditionnalInclude_Cookie($incl)
	{
		if ( preg_match('/^\\[(!)?Cookie\.([a-zA-Z0-9_-]+)\\]$/', $incl, $regs) )
		{
			$test = array_key_exists($regs[2], $_COOKIE) && $_COOKIE[$regs[2]];
			if ( $regs[1] == '!' )
				return !$test;
			else
				return $test;
		}
		else
			return false;
	}
	
	
	
	/**
	 * Tester la présence d'une directive pour une inclusion conditionnelle
	 *
	 * @param string $incl Chemin d'inclusion à tester
	 * @return bool Renvoie true si le chemin testé est une directive
	 */
	protected function conditionnalInclude($incl)
	{
		return (substr($incl, 0, 1) == '[') && (substr($incl, -1) == ']');
	}
	
	
	
	/**
	 * Faire l'inventaire des resources CSS/JS, éventuellement en traitant les directives d'inclusion conditionnelles, et obtenir une liste de ressources
	 *
	 * @param string[] $refs Liste de références CSS ou JS
	 * @return string[] Renvoie une liste de ressources traitée (chemins, directives, etc.)
	 */
	protected function listCSSJS(array $refs)
	{
		$ret = [];
		
		
		foreach ( $refs as $incl )
		{
			// si valeur est un tableau
			if ( is_array($incl) )
			{
				// si première valeur du tableau est une directive d'inclusion conditionnelle, la traiter et opérer pour le reste par récursion
				if ( $this->conditionnalInclude($incl[0]) )
				{
					if ( $this->conditionnalInclude_Cookie($incl[0]) )
						$ret = array_merge($ret, $this->listCSSJS(array_slice($incl, 1)));
				}


				// sinon, le premier élément est le chemin et les autres sont les fichiers à inclure
				else
				{
					$path = $incl[0];
					$tmp = $this->listCSSJS(array_slice($incl, 1));
					
					// retraiter les inclusions obtenues dans $tmp pour rajouter en préfixe $path
					$ret = array_merge($ret, array_map(function($inc) use ($path){ return "$path/$inc"; }, $tmp));
				}
			}
			else
				$ret[] = $incl;
		}
		
		
		return $ret;
	}
	
	
	
	/**
	 * Inclure des resources JS ou CSS sous forme d'objets HtmlReference, et augmenter un accumulateur avec le code HTML correspondant
	 *
	 * @param HtmlReference[] $objs Liste d'objets Reference
	 * @param string $kind Type de resource recherchée (CSS ou JS)
	 * @param object $data Accumulateur des ressources CSS et JS en html
	 */
	protected function includeReferenceObjects(array $objs, $kind, $data)
	{
		foreach ( $objs as $incl )
			$data->$kind[] = $incl->getHtml();
	}
	
	
	
	/**
	 * Inclure des resources CSS ou JS depuis un manifest donné et augmenter un accumulateur avec le code HTML correspondant
	 *
	 * @param \Ppast\App_Framework\Manifest $m Manifest à considérer
	 * @param \Ppast\App_Framework\Name_space $ns Namespace courant app/ctx/page
	 * @param string $kind Type de resource recherchée (CSS ou JS)
	 * @param string $manifestKey Clef à rechercher dans le manifest : CSS, CSS_NO_CACHE, JS, JS_NO_CACHE
	 * @param object $data Accumulateur des ressources CSS et JS en html
	 */
	protected function includeCSSJSFromManifest(Manifest $m, Name_space $ns, $kind, $manifestKey, $data)
	{
		// traiter les inclusions JS
		$rm = new \ReflectionClass(Manifest::class);
		$k = $rm->getConstant($manifestKey);

		if ( $res = $m->$k )
		{
			if ( !is_array($res) )
				$res = array($res);
			
			
			// créer liste d'objet HtmlReference
			$objs = $this->createHtmlReferenceObjects($res, $ns, $kind);
			$this->includeReferenceObjects($objs, $kind, $data);
		}			
	}
	
	
	
	/**
	 * Fusionner des resources CSS/JS : obtenir une liste d'objets HtmlReference
	 *
	 * @param string[] $refs Liste de références CSS ou JS
	 * @param \Ppast\App_Framework\Name_space $ns Namespace courant app/ctx/page
	 * @param string $kind Contient CSS ou JS
	 * @return Ppast\App_Framework\Bootstrap\PageRes\HtmlReference[] $liste Contient la liste des ressources, construite au fur et à mesure des appels récursifs
	 */
	protected function createHtmlReferenceObjects(array $refs, Name_space $ns, $kind)
	{
		// lister les ressources de façon récursive
		$res = $this->listCSSJS($refs);

		// créer les objets ressource
		$ret = [];
		$rclass = new \ReflectionClass('\\Ppast\\App_Framework\\Bootstrap\\PageRes\\' . $kind . 'Include');
		foreach ( $res as $inc )
			$ret[] = $rclass->newInstance($inc, $ns);
		
		return $ret;
	}
	
	
	
	/**
	 * Fusionner des resources CSS/JS depuis un manifest donné 
	 *
	 * @param \Ppast\App_Framework\Manifest $m Manifest à considérer
	 * @param \Ppast\App_Framework\Name_space $ns Namespace courant app/ctx/page
	 * @param string $kind Contient CSS ou JS
	 * @param Cache $minifyCache Cache pour minification ou false pour si optimisation désactivée
	 * @param object $data Accumulateur des ressources CSS et JS en html
	 */
	protected function mergeCSSJSFromManifest(Manifest $m, Name_space $ns, $kind, Cache $minifyCache, $data)
	{
		// traiter les inclusions CSS ou JS selon $kind
		$rm = new \ReflectionClass(Manifest::class);
		$k = $rm->getConstant($kind);
		if ( $res = $m->$k )
		{
			if ( !is_array($res) )
				$res = array($res);


			// créer les objets JSInclude ou CSSInclude
			$objs = $this->createHtmlReferenceObjects($res, $ns, $kind);
			
			
			// fusionner toutes les ressources du manifest, et récupérer les ressources ignorées (références externes http)
			$ignored = [];
			$merged = $minifyCache->merge($objs, $ns, $kind, $ignored);
			
			// si fusion a donné un fichier, l'ajouter dans l'accumulateur $data
			if ( $merged )
				$this->includeReferenceObjects([$merged], $kind, $data);
			
			// si des fichiers ignorés (car références externes, les ajouter maintenant comme des ressources normales, non en cache)
			if ( count($ignored) )
				$this->includeReferenceObjects($ignored, $kind, $data);
		}			
	}
	
	
	
	/** 
	 * Traiter les inclusions CSS ou JS, en fonction des arguments
	 *
	 * @param \Ppast\App_Framework\Manifest $m Manifest à considérer
	 * @param \Ppast\App_Framework\Name_space $ns Namespace courant app/ctx/page
	 * @param string $kind Contient CSS ou JS
	 * @param false|Cache $minifyCache Cache pour minification ou false pour si optimisation désactivée
	 * @param object $data Accumulateur des ressources CSS et JS en html
	 */
	protected function includeCSSJS(Manifest $m, Name_space $ns, $kind, $minifyCache, $data)
	{
		if ( $minifyCache )
			$this->mergeCSSJSFromManifest($m, $ns, $kind, $minifyCache, $data);
		else
			// sinon, traiter les inclusions JS une à une
			$this->includeCSSJSFromManifest($m, $ns, $kind, $kind, $data);

		
		// traiter les inclusions CSS/JS qui ne peuvent être mises en cache
		$this->includeCSSJSFromManifest($m, $ns, $kind, $kind . '_NO_CACHE', $data);
	}
	
	
	
	/**
	 * Obtenir un objet d'affichage de feedback
	 *
	 * @param \Ppast\App_Framework\Name_space $ns Namespace courant app/ctx/page
	 * @throws \Ppast\App_Framework\Bootstrap\Exceptions\ValueError Exception levée si le gestionnaire de feedback n'est pas de la classe attendue
	 * @return \Ppast\App_Framework\FeedbackHandlers\Handler Renvoie une instance d'affichage de feedback
	 */
	protected function getFeedbackHandler(Name_space $ns)
	{
		// si gestionnaire de feedback, faire le feedback ici ; sinon, ce sera de la responsabilité du PageContainer
		$fbclass = Bootstrap::getManifestFinalValue($ns, Manifest::FEEDBACK_HANDLER, \Ppast\App_Framework\FeedbackHandlers\Simple::class);
		$fb = new $fbclass();
		if ( !($fb instanceof \Ppast\App_Framework\FeedbackHandlers\Handler) )
			throw new \Ppast\App_Framework\Bootstrap\Exceptions\ValueError("Le gestionnaire de feedback n'hérite pas de Ppast\\App_Framework\\FeedbackHandlers\\Handler");
		
		return $fb;
	}
	
	
	
	/** 
	 * Définir le titre depuis le manifest
	 *
	 * @param \Ppast\App_Framework\Name_space $ns Namespace courant app/ctx/page
	 */
	public function setTitleFromManifest(Name_space $ns)
	{
		if ( $mpage = Manifest::getManifest($ns, Manifest::MANIFEST_PAGE) )
		{
			// traiter le titre
			if ( $title = $mpage->{Manifest::TITLE} )
				$this->setTitle($title);
		}
	}
	
	
	
	/**
	 * Définir les inclusions CSS et JS 
	 *
	 * @param \Ppast\App_Framework\Name_space $ns Namespace courant app/ctx/page
	 * @param \Ppast\App_Framework\Request $req Requête courante
	 */
	public function setCSSJSIncludes(Name_space $ns, Request $req)
	{
		// créer le cache CSS/JS, si activé
		$minifyCache = Cache::isActive($ns) ? new Cache($ns) : false;
			
		
		$data = (object)[
			'CSS'			=> [],
			'JS'			=> []
		];
				

		
		// traiter au niveau du manifest ctx et page
		$mkinds = array(Manifest::MANIFEST_CONTEXT, Manifest::MANIFEST_PAGE);
		foreach( $mkinds as $mkind )
		{
			// lire le manifest
			if ( $m = Manifest::getManifest($ns, $mkind) )
			{
				$this->includeCSSJS($m, $ns, 'CSS', $minifyCache, $data);
												
				// si inclusion JS seulement si commande, ou si option désactivée
				if ( (!$m->{Manifest::JS_INCL_IFCOMMAND}) || ($m->{Manifest::JS_INCL_IFCOMMAND} && $req->command) )
					$this->includeCSSJS($m, $ns, 'JS', $minifyCache, $data);
			}
		}
		
		
		// quand on arrive ici, on a dans $data une liste de code HTML d'inclusion des ressources CSS et JS
		$this->setJSIncludes(implode("\n", $data->JS));
		$this->setCSSIncludes(implode("\n", $data->CSS));
	}
	
	
	
	/**
	 * Envoyer le contenu du template dans la sortie standard
	 *
	 * @param \Ppast\App_Framework\Name_space $ns Namespace courant app/ctx/page
	 * @param \Ppast\App_Framework\Request $req Requête courante
	 * @throws \Ppast\App_Framework\Bootstrap\Exceptions\NotFound Exception levée si un tag requis n'est pas trouvé dans le template
	 */
	public function output(Name_space $ns, Request $req)
	{
		// obtenir une instance d'affichage de feedback
		$fb = $this->getFeedbackHandler($ns);	
		
		// rechercher le tag body, et afficher tout ce qu'il y a avant
		$bodytag = self::_htmlTag(self::BODY_TAG);
		$pos = strpos($this->value, $bodytag);
		
		if ( $pos === false )
			throw new \Ppast\App_Framework\Bootstrap\Exceptions\NotFound("Impossible de trouver le tag '" . self::BODY_TAG . "' dans le modèle");
		
		
		// ecrire contenu avant Body
		echo substr($this->value, 0, $pos);
		
		// écrire feedback
		$fb->display($req);
		
		// effectuer le rendu de la page
		Application::render();

		// écrire fin du modèle
		echo substr($this->value, $pos + strlen($bodytag));
	}
	
	
	
	/**
	 * Générer le modèle, qui s'écrit dans la sortie standard
	 *
	 * @param \Ppast\App_Framework\Name_space $ns Namespace courant app/ctx/page
	 * @param \Ppast\App_Framework\Request $req Requête courante
	 */
	public function render(Name_space $ns, Request $req)
	{
		// définir titre depuis manifest
		$this->setTitleFromManifest($ns);
		
		// traiter inclusions CSS et JS, avec gestion minify éventuelle
		$this->setCSSJSIncludes($ns, $req);
				
		// envoyer la génération dans la sortie standard
		$this->output($ns, $req);
	}
	
}

?>

