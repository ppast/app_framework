<?php

namespace Ppast\App_Framework\Bootstrap\PageRes;


use \Ppast\App_Framework\Name_space;
use \Ppast\App_Framework\Request;
use \Ppast\App_Framework\Manifest;
use \Ppast\App_Framework\Utils;
use \Ppast\App_Framework\Bootstrap;
use \Ppast\App_Framework\Application;



// classe pour include CSS
class CSSInclude extends HtmlReference
{
	/**
	 * Inclure le fichier CSS référencé : obtenir code HTML
	 */
	public function getHtml()
	{
		// calculer le chemin réel depuis racine web
		return "<link href=\"" . $this->buildPath() . "\" rel=\"stylesheet\">";
	}
}

?>