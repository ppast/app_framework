<?php

namespace Ppast\App_Framework\Bootstrap\SecurityHandlers;



// gestionnaire de sécurité composite
class Composite extends Base
{
	// [---- DECL. PROTEGEES ----
	
	// gestionnaire de sécurité imbriqués (composite)
	protected $_innerHandlers = array();

	// ---- DECL. PROTEGEES -----]


	/**
	 * Définir les gestionnaires de sécurité rassemblés par ce composite
	 *
	 * @param \Ppast\App_Framework\Bootstrap\SecurityHandlers\Handler[]
	 */
	function setInnerHandlers($handlers)
	{
		$this->_innerHandlers = $handlers;
	}

	
	
	/**
	 * Autoriser une connexion
	 * 
	 * @param \Ppast\App_Framework\Sessions\Session $s Session en cours
	 * @throws \Ppast\App_Framework\Bootstrap\Exceptions\Auth Exception levée en cas d'erreur d'autorisation
	 */
	function authorize(\Ppast\App_Framework\Sessions\Session $s)
	{
		// envoyer vérification aux éléments composite
		foreach ( $this->_innerHandlers as $h )
			$h->authorize($s);
	}


	
	/** 
	 * Initialiser une connexion 
	 * 
	 * Définir ici les valeurs nécessaires pour vérifier la sécurité de la connexion ultérieurement ; généralement, 
	 * il s'agit de créer un jeton ou une session, et d'utiliser $logindata pour extraire le nom de l'utilisateur ou son mot de passe
	 *
	 * @param \Ppast\App_Framework\Sessions\Session $s Session en cours
	 * @param array $logindata Informations de login en tableau associatif
	 */
	function initialize(\Ppast\App_Framework\Sessions\Session $s, $logindata)
	{
		// envoyer init aux éléments composites
		foreach ( $this->_innerHandlers as $h )
			$h->initialize($s, $logindata);
	}
	
	
	
	/**
	 * Définir l'url nécessaire pour démarrer l'application par un lien
	 * 
	 * Certains gestionnaires peuvent nécessiter des paramètres URL, comme CSRF, qui seront rajoutés ici
	 *
	 * @param \Ppast\App_Framework\Sessions\Session $s Session en cours
	 * @param $url Url de boot passée par référence
	 */
	function bootUrl(\Ppast\App_Framework\Sessions\Session $s, &$url)
	{
		// envoyer demande de boot aux éléments composites
		foreach ( $this->_innerHandlers as $h )
			$h->bootUrl($s, $url);
	}
	
	

	/**
	 * Tester la présence des paramètres nécessaires dans $this->params
	 * 
	 * @throws \Ppast\App_Framework\Bootstrap\Exceptions\NotFound Exception levée en cas d'absence d'un paramètre obligatoire à la construction du gestionnaire de sécurité
	 */
	function testParameters()
	{
		// envoyer vérification aux éléments composite
		foreach ( $this->_innerHandlers as $h )
			$h->testParameters();
	}	
}



?>