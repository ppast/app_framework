<?php

namespace Ppast\App_Framework\Bootstrap\SecurityHandlers;

use \Ppast\App_Framework\Bootstrap;



// gestionnaire de sécurité pour vérifier que le user possède au moins un rôle parmi une liste de rôles admissibles
class Role extends Base
{
	// [---- DECL. STATIQUES ----
	
	// paramètres de ce gestionnaire
	const P_ACCEPTABLE_ROLES = 'role_sh_acceptable_roles';					// tableau des rôles admissibles
	
	// ---- DECL. STATIQUES ----]
	
    
	/**
	 * Construire le gestionnaire de sécurité
	 *
	 * @param string[] $roles Liste des rôles admissibles
	 */
	public function __construct($roles)
	{
		parent::__construct([self::P_ACCEPTABLE_ROLES => $roles]);
	}
	
	

	/**
	 * Autoriser une connexion
	 * 
	 * @param \Ppast\App_Framework\Sessions\Session $s Session en cours
	 * @throws \Ppast\App_Framework\Bootstrap\Exceptions\Auth Exception levée en cas d'erreur d'autorisation
	 */
	function authorize(\Ppast\App_Framework\Sessions\Session $s)
	{
        // vérifier qu'on a bien un security handler qui gère les users
		$sh = Bootstrap::getSecurityHandler('Users');
		if ( is_null($sh) )
			throw new \Ppast\App_Framework\Bootstrap\Exceptions\Auth('Il n\'y a pas de gestionnaire de sécurité \'Users\' déclaré dans le manifest de l\'application');

        
        // obtenir provider (par construction, il existe)
        $provider = $sh->params->{Users::P_USERS_SH_PROVIDER};
        
        // obtenir user (depuis stockage du username dans la session)
        $uname = $s->get(User::USER_SH_NAME);
        $u = $provider->getUser($uname);
        
        // vérifier existence user
        if ( !$u )
            throw new \Ppast\App_Framework\Bootstrap\Exceptions\Auth("L'utilisateur '$uname' n'existe pas.");
        
        // si u->roles n'existe pas, pb
        if ( !property_exists($u, 'roles') )
			throw new \Ppast\App_Framework\Bootstrap\Exceptions\Auth("La définition de l'utilisateur '$uname' ne communique pas la propriété 'roles'.");

        
		// parmi tous les roles admissibles, en trouver au moins un possédé par le user
        $rolesok = $this->params->{self::P_ACCEPTABLE_ROLES};
        $uroles = explode(',', $u->roles);
		foreach ( $rolesok as $r )
			if ( in_array($r, $uroles) )
				return; // ok, un role est trouvé

		
        // si on arrive ici, problème, aucun rôle admissible trouvé
        throw new \Ppast\App_Framework\Bootstrap\Exceptions\Auth("L'utilisateur '$uname' ne détient pas le rôle requis (" . implode(', ', $rolesok) . ") pour accéder à cette page.");
	}

	
	
	/** 
	 * Initialiser une connexion 
	 * 
	 * Définir ici les valeurs nécessaires pour vérifier la sécurité de la connexion ultérieurement ; généralement, 
	 * il s'agit de créer un jeton ou une session, et d'utiliser $logindata pour extraire le nom de l'utilisateur ou son mot de passe
	 *
	 * @param \Ppast\App_Framework\Sessions\Session $s Session en cours
	 * @param array $logindata Informations de login en tableau associatif
	 */

	function initialize(\Ppast\App_Framework\Sessions\Session $s, $logindata)
	{
	}

	
	
	/**
	 * Tester la présence des paramètres nécessaires dans $this->params
	 * 
	 * @throws \Ppast\App_Framework\Bootstrap\Exceptions\NotFound Exception levée en cas d'absence d'un paramètre obligatoire à la construction du gestionnaire de sécurité
	 */
	function testParameters()
	{
		parent::testParameters();
			
		$this->_doTestParameters(array(self::P_ACCEPTABLE_ROLES));
	}	
}

?>