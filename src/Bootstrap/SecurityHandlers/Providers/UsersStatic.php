<?php

namespace Ppast\App_Framework\Bootstrap\SecurityHandlers\Providers;



// provider utilisateurs sous forme de tableau statique ASSOCIATIF
class UsersStatic implements UsersInterface
{
    protected $_users = NULL;
    
    
    /**
	 * Obtenir un utilisateur et renvoyer un littéral objet avec des propriétés (user, passwd, roles, etc.)
	 *
	 * @param string $u
	 * @return object
	 */
    public function getUser($u)
    {
        // chercher l'utilisateur
        $user = $this->_users[$u];
        
        if ( !$user )
            return FALSE;
        
        // si user est sous forme de littéral objet, renvoyer tel quel, sinon convertir tableau associatif en littéral objet
        if ( !is_object($user) )
            $user = (object)$user;
        
        return $user;
    }
    
    
    /**
	 * Constructeur
	 *
	 * @param array Tableau associatif d'utilisateur et leurs propriétés sous forme de littéral objet
	 */
    public function __construct($users)
    {
        $this->_users = $users;
    }
}

?>