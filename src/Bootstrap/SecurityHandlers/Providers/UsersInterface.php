<?php

namespace Ppast\App_Framework\Bootstrap\SecurityHandlers\Providers;



// interface pour provider utilisateurs
interface UsersInterface
{
    /**
	 * Obtenir un utilisateur et renvoyer un littéral objet avec des propriétés (user, passwd, roles, etc.)
	 *
	 * @param string $u
	 * @return object
	 */
    function getUser($u);
}

?>