<?php

namespace Ppast\App_Framework\Bootstrap\SecurityHandlers\Providers;



// provider utilisateurs sous forme de fichier CSV (;)
class UsersFile extends UsersStatic
{
    /**
	 * Constructeur
	 * 
	 * @param string $filename Chemin vers fichier de définition des utilisateurs
	 */
    public function __construct($filename)
    {
		if ( !file_exists($filename) )
			throw new \Ppast\App_Framework\Bootstrap\Exceptions\NotFound("Fichier de configuration des utilisateurs introuvable");
			
		// ouvrir le fichier des utilisateurs
		$f = fopen($filename, 'r');
		$ret = array();
        
        
        // lire définition des propriétés
        $props = trim(fgets($f));
        if ( !$props )
			throw new \Ppast\App_Framework\Bootstrap\Exceptions\ValueError("Le fichier de configuration des utilisateurs est vide");

        $props = array_map('trim', explode(";", $props));
        
        
        // lire users
		while ( $s = trim(fgets($f)) )
		{
			$utmp = array_map('trim', explode(";", $s));
            $user = array();
            $i = 0;
            
            // transformer la lecture indexée en tableau associatif propriété=valeur
            foreach ( $props as $k )
                $user[$k] = $utmp[$i++];
            
            // par construction, le user est toujours la première info lue
            $ret[$utmp[0]] = (object)$user;
		}
        
        fclose($f);
        
		
        // appeler le constructeur parent (tableau statique indexé)
        parent::__construct($ret);
    }
}

?>