<?php

namespace Ppast\App_Framework\Bootstrap\SecurityHandlers;



// gestionnaire de sécurité pour présence utilisateur
class User extends Base
{
	// [---- DECL. STATIQUES ----
	
	// marqueur de session
	const USER_SH_NAME = 'user_sh_name';							// nom d'utilisateur
	
	// ---- DECL. STATIQUES ----]


	/**
	 * Autoriser une connexion
	 * 
	 * @param \Ppast\App_Framework\Sessions\Session $s Session en cours
	 * @throws \Ppast\App_Framework\Bootstrap\Exceptions\Auth Exception levée en cas d'erreur d'autorisation
	 */

	function authorize(\Ppast\App_Framework\Sessions\Session $s)
	{
		if ( !$s->get(self::USER_SH_NAME) )
			throw new \Ppast\App_Framework\Bootstrap\Exceptions\Auth('Utilisateur absent.');
	}

	

	/** 
	 * Initialiser une connexion 
	 * 
	 * Définir ici les valeurs nécessaires pour vérifier la sécurité de la connexion ultérieurement ; généralement, 
	 * il s'agit de créer un jeton ou une session, et d'utiliser $logindata pour extraire le nom de l'utilisateur ou son mot de passe
	 *
	 * @param \Ppast\App_Framework\Sessions\Session $s Session en cours
	 * @param array $logindata Informations de login en tableau associatif
	 */
	function initialize(\Ppast\App_Framework\Sessions\Session $s, $logindata)
	{
		$s->set(self::USER_SH_NAME, $logindata[self::USER_SH_NAME]);
	}
}


?>