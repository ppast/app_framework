<?php

namespace Ppast\App_Framework\Bootstrap\SecurityHandlers;



// gestionnaire de sécurité pour protection CSRF
class CSRF extends Base
{
	// [---- DECL. STATIQUES ----

	// nom de la valeur CSRF en requete
	const CSRF_SH_URL_PARAM_NAME = '__CSRF__';
	
	// ---- DECL. STATIQUES ----]
	
	
	// [---- DECL. PROTEGEES ----

	/**
	 * Forger la valeur CSRF aléatoire
	 *
	 * @return string
	 */
	protected function _createCSRFValue()
	{
		return bin2hex(random_bytes(32));
	}
	
	
	// ---- DECL. PROTEGEES ----]


	
	// valeurs CSRF
	public $CSRF_Value = NULL;


	
	/**
	 * Ecrire un champ caché de formulaire dans la sortie standard
	 */
	function writeHiddenCSRFField()
	{
		echo "<input type=\"hidden\" name=\"" . self::CSRF_SH_URL_PARAM_NAME . "\" value=\"" . $this->CSRF_Value . "\">\n";
	}
	
	
	/**
	 * Ajouter la valeur CSRF au bout d'une url
	 * 
	 * @param string $url
	 * @return string
	 */
	function addCSRFToUrl($url)
	{
		// si déjà des paramètres, rajouter à la fin le jeton
		return $url . (strpos($url, '?') ? '&':'?') . self::getCSRFUrl();
	}
	
		
	
	/**
	 * Fournir une valeur hashée du cookie CSRF (pour ne pas divulguer la valeur réelle dans une URL get, l'historique du navigateur etc.)
	 *
	 * @return string
	 */
	function hashedCSRFValue()
	{
		return '!' . hash_hmac('sha256', $this->CSRF_Value, date('Y'));
	}


	
	/**
	 * Obtenir une chaîne key=valeur_CSRF prête à être utilisée dans une URL
	 *
	 * @return string
	 */
	function getCSRFUrl()
	{
		return self::CSRF_SH_URL_PARAM_NAME . "=" . $this->hashedCSRFValue();
	}


	
	/**
	 * Définir l'url nécessaire pour démarrer l'application par un lien
	 * 
	 * Ici, on rajoute dans l'url en argument la valeur csrf
	 *
	 * @param \Ppast\App_Framework\Sessions\Session $s Session en cours
	 * @param $url Url de boot passée par référence
	 */
	function bootUrl(\Ppast\App_Framework\Sessions\Session $s, &$url)
	{
		$url = $this->addCSRFToUrl($url);
	}
	
	

	/**
	 * Autoriser une connexion
	 * 
	 * @param \Ppast\App_Framework\Sessions\Session $s Session en cours
	 * @throws \Ppast\App_Framework\Bootstrap\Exceptions\Auth Exception levée en cas d'erreur d'autorisation
	 */
	function authorize(\Ppast\App_Framework\Sessions\Session $s)
	{
		// vérifier présence cookie CSRF
		$this->CSRF_Value = $s->storageInterface->{$s->name . self::CSRF_SH_URL_PARAM_NAME};
		if ( !$this->CSRF_Value )
			throw new \Ppast\App_Framework\Bootstrap\Exceptions\Auth("Valeur CSRF inexistante dans le stockage du navigateur.");
		

		// vérifier présence valeur CSRF dans la requete
		$p = $s->requestInterface->{self::CSRF_SH_URL_PARAM_NAME};
		if ( !$p )
			throw new \Ppast\App_Framework\Bootstrap\Exceptions\Auth("Valeur CSRF inexistante dans la requête du navigateur.");
		
				
		// déterminer si valeur soumise dans la requête est la valeur CSRF telle quelle (POST) ou une valeur hashée (GET)
		if ( strpos($p, '!') === 0 )
		{
			if ( !hash_equals($this->hashedCSRFValue(), $p) )
				throw new \Ppast\App_Framework\Bootstrap\Exceptions\Auth("Valeur CSRF discordantes.");
		}
		else
			if ( !hash_equals($this->CSRF_Value, $p) )
				throw new \Ppast\App_Framework\Bootstrap\Exceptions\Auth("Valeur CSRF discordantes.");
	}

	
	
	
	/** 
	 * Initialiser une connexion 
	 * 
	 * Définir ici les valeurs nécessaires pour vérifier la sécurité de la connexion ultérieurement ; généralement, 
	 * il s'agit de créer un jeton ou une session, et d'utiliser $logindata pour extraire le nom de l'utilisateur ou son mot de passe
	 *
	 * @param \Ppast\App_Framework\Sessions\Session $s Session en cours
	 * @param array $logindata Informations de login en tableau associatif
	 */
	function initialize(\Ppast\App_Framework\Sessions\Session $s, $logindata)
	{
		// définir cookie CSRF
		$this->CSRF_Value = $s->storageInterface->{$s->name . self::CSRF_SH_URL_PARAM_NAME} = $this->_createCSRFValue();
	}
}


?>