<?php

namespace Ppast\App_Framework\Bootstrap\SecurityHandlers;



// gestionnaire de sécurité pour utilisateurs énumérés
class Users extends Base
{
	// [---- DECL. STATIQUES ----
	
	// paramètres de ce gestionnaire
	const P_USERS_SH_PROVIDER = 'users_sh_provider';					// fournisseur pour liste des users

	// valeurs de session liées au gestionnaire
	const USERS_SH_USER_PASSWORD = 'users_sh_user_password';
	
	// ---- DECL. STATIQUES ----]

	
	/**
	 * Construire le gestionnaire de sécurité
	 *
	 * @param Providers\UsersInterface $provider Objet stratégie pour fournisseur de liste d'utilisateurs
	 */
	public function __construct($provider)
	{
		parent::__construct([self::P_USERS_SH_PROVIDER => $provider]);
	}
	
	

	/**
	 * Autoriser une connexion
	 * 
	 * @param \Ppast\App_Framework\Sessions\Session $s Session en cours
	 * @throws \Ppast\App_Framework\Bootstrap\Exceptions\Auth Exception levée en cas d'erreur d'autorisation
	 */
	function authorize(\Ppast\App_Framework\Sessions\Session $s)
	{
        // obtenir provider (par construction grâce à testParameters, il existe)
        $provider = $this->params->{self::P_USERS_SH_PROVIDER};
        
        // obtenir user (depuis stockage du username dans la session)
        $uname = $s->get(User::USER_SH_NAME);
        $u = $provider->getUser($uname);
        
        // si user inexistant
        if ( !$u )
           throw new \Ppast\App_Framework\Bootstrap\Exceptions\Auth("Utilisateur '$uname' inexistant");
        
		
        // tester password ; si correspondance, tout va bien
        if ( $u->password != $s->get(self::USERS_SH_USER_PASSWORD) )
            throw new \Ppast\App_Framework\Bootstrap\Exceptions\Auth("Identification incorrecte");
	}

	

	/** 
	 * Initialiser une connexion 
	 * 
	 * Définir ici les valeurs nécessaires pour vérifier la sécurité de la connexion ultérieurement ; généralement, 
	 * il s'agit de créer un jeton ou une session, et d'utiliser $logindata pour extraire le nom de l'utilisateur ou son mot de passe
	 *
	 * @param \Ppast\App_Framework\Sessions\Session $s Session en cours
	 * @param array $logindata Informations de login en tableau associatif
	 */
	function initialize(\Ppast\App_Framework\Sessions\Session $s, $logindata)
	{
		$s->set(self::USERS_SH_USER_PASSWORD, $logindata[self::USERS_SH_USER_PASSWORD]);
	}
	
	
	
	/**
	 * Tester la présence des paramètres de construction nécessaires dans $this->params
	 * 
	 * @throws \Ppast\App_Framework\Bootstrap\Exceptions\NotFound Exception levée en cas d'absence d'un paramètre obligatoire à la construction du gestionnaire de sécurité
	 */
	function testParameters()
	{
        // appel test parent
		parent::testParameters();
        
        
        // tester les paramètres de cette classe : le provider des utilisateurs doit être renseigné
        $this->_doTestParameters(array(self::P_USERS_SH_PROVIDER));
        
        
        // tester qu'il définisse bien l'interface Providers\UsersInterface
		if ( !(new \ReflectionClass($this->params->{self::P_USERS_SH_PROVIDER}))->isSubclassOf(\Ppast\App_Framework\Bootstrap\SecurityHandlers\Providers\UsersInterface::class) )
			throw new \Ppast\App_Framework\Bootstrap\Exceptions\NotFound("Paramètre '" . self::P_USERS_SH_PROVIDER . "' n'hérite pas de 'Ppast\App_Framework\Bootstrap\SecurityHandlers\Providers\UsersInterface'.");
	}	
}


?>