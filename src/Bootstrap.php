<?php

namespace Ppast\App_Framework;



// gestionnaire de démarrage
class Bootstrap
{
	// [---- DECL. PRIVEES ----
	
	const SESSION_NAME_URL = '__session__';			// nom de la session forcé par URL ; ne pas indiquer dans le manifest, mais dans l'url !
	
	
	
	/**
	 * Obtenir la session courante
	 *
	 * @param \Ppast\App_Framework\Name_space $ns Namespace courant app/ctx/page
	 * @throws \Ppast\App_Framework\Bootstrap\Exceptions\NotFound Exception levée si des manifest ou valeurs de manifest n'existent pas
	 */
	private static function _getSession(Name_space $ns)
	{
		// lire le manifest application et le manifest contexte
		$mapp = Manifest::getManifest($ns, Manifest::MANIFEST_APP);
		if ( !$mapp )		
			throw new Bootstrap\Exceptions\NotFound("Manifest application inexistant");

		$mctx = Manifest::getManifest($ns, Manifest::MANIFEST_CONTEXT);
		if ( !$mctx )		
			throw new Bootstrap\Exceptions\NotFound("Manifest contexte inexistant");

		// lire classe de session et nom appli
		$sclass = $mapp->{Manifest::SESSION_CLASS};
		if ( !$sclass )
			throw new Bootstrap\Exceptions\NotFound("Classe de session inconnue");

		$sname = $mctx->{Manifest::SESSION_NAME};
		if ( !$sname )
			throw new Bootstrap\Exceptions\NotFound("Nom session inconnu");
	
		
		// créer la session avec la classe demandée et le nom de l'application
		return (new \ReflectionClass($sclass))->newInstance($sname);
	}

	// ---- DECL. PRIVEES ----]


	// [---- DECL. PUBLIQUES ----
	
	// session courante
	static $session = NULL;
	static $namespace = NULL;
	static $root = NULL;			/* racine sur serveur	: ex. "/homez.18/assistano/www" ; ne se termine pas par / */
	static $approot = '';			/* chemin appli depuis racine serveur ; ne commence PAS par / mais finit par /, sauf si vide */
	
	
    
	/**
	 * Obtenir le nom de la session depuis un paramètre de requête
	 * 
	 * Le nom de la session est en principe donné par SESSION_NAME dans le manifeste du contexte.
	 * Cependant, dans le cas où un fichier est partagé entre plusieurs contextes, la session doit être identifiée autrement
	 *
	 * @return string
	 */	
	static function getSessionNameFromUrl()
	{
		return $_REQUEST[self::SESSION_NAME_URL];
	}
	
	
	
	/**
	 * Obtenir user actif depuis session
	 * 
	 * @return object Renvoie littéral objet décrivant l'utilisateur
	 * @throws \Ppast\App_Framework\Bootstrap\Exceptions\NotFound Exception levée si le gestionnaire de sécurité Users n'a pas été trouvé ou si l'utilisateur n'existe pas
	 */
	static function getCurrentUser()
	{
        // obtenir security handler USERS
		$sh = Bootstrap::getSecurityHandler('Users');
		if ( is_null($sh) )
			 throw new Bootstrap\Exceptions\NotFound('Il n\'y a pas de gestionnaire de sécurité \\Ppast\\App_Framework\\Bootstrap\\SecurityHandlers\\Users déclaré dans le manifest de l\'application');
        
        
        // obtenir provider (par construction, il existe)
        $provider = $sh->params->{Bootstrap\SecurityHandlers\Users::P_USERS_SH_PROVIDER};

        
        // obtenir user (depuis stockage du username dans la session)
        $uname = self::$session->get(Bootstrap\SecurityHandlers\User::USER_SH_NAME);
        $u = $provider->getUser($uname);
        
        
        // si user inexistant, exception
        if ( !$u )
            throw new Bootstrap\Exceptions\NotFound("Utilisateur '$uname' inexistant");
        
        return $u;
    }
	
    

	/**
	 * Obtenir la valeur finale d'une valeur de manifest, en rafinant depuis manifests application -> contexte -> page
	 *
	 * On renvoie la dernière valeur définie, ce qui permet au manifest page ou contexte de surcharger la définition du manifest application
	 *
	 * @param \Ppast\App_Framework\Name_space $ns Namespace courant app/ctx/page
	 * @param string $key Valeur demandée
	 * @param mixed $default Valeur par défaut à renvoyer si $key n'existe pas
	 * @return mixed Renvoie la valeur $key lue dans le dernier manifest dans l'ordre app->ctx->page
	 */	 
	static function getManifestFinalValue(Name_space $ns, $key, $default = NULL)
	{
		// explorer les manifests dans cet ordre
		$kinds = array(Manifest::MANIFEST_APP, Manifest::MANIFEST_CONTEXT, Manifest::MANIFEST_PAGE);
		
		// pour l'instant, on renvoie la valeur par défaut
		$ret = $default;
		
		foreach ( $kinds as $kind )
		{
			// includes au niveau de l'appli, contexte ou page
			$m = Manifest::getManifest($ns, $kind);
			if ( $m && ($val = $m->$key) )
				$ret = $val; // continuer à boucler pour éventuellement trouver une valeur dans un manifest plus proche de la page
		}
		
		return $ret;
	}
	

    
	/**
	 * Obtenir un gestionnaire de sécurité
	 *
	 * @param string $classname
	 * @return null|\Ppast\App_Framework\Bootstrap\SecurityHandlers\Handler Renvoie le gestionnaire de classe $classname ou null si inexistant
	 */
	static function getSecurityHandler($classname)
	{
		return Bootstrap\SecurityProcessor::getSecurityHandler($classname);
	}

    
    
	/**
	 * Initialiser le bootstrap
	 * 
	 * @param string $ns Namespace app/ctx/page sous forme de chaine app_ctx_page
	 */
	static function init($ns)
	{
		// obtenir des infos sur le namespace (app_ctx_page)
		self::$namespace = Name_space::fromString($ns);
		self::$root = Utils::documentRoot();
	}
	
    
	
	/**
	 * Obtenir un paramètre de config depuis le manifest app
	 *
	 * @param string $key Valeur de configuration à rechercher
	 * @param mixed $defvalue Valeur par défaut à renvoyer
	 * @param bool $exception_if_missing Si valeur manquante et cet argument vaut true, une exception sera levée, sinon la valeur par défaut sera retournée
	 * @throws \Ppast\App_Framework\Bootstrap\Exceptions\NotFound Exception levée si valeur demandée n'existe pas et argument $exception_if_missing = true
	 */
	static function config($key, $defvalue = NULL, $exception_if_missing = false)
	{
		// lire manifest app
		$mapp = Manifest::getManifest(self::$namespace, Manifest::MANIFEST_APP); 
		if ( !$mapp )
			if ( $exception_if_missing )
				throw new Bootstrap\Exceptions\NotFound("Manifest application illisible");
			else
				return $defvalue;
			
		
		// si tableau de config existe
		if ( $cfg = $mapp->{Manifest::CONFIG} )
			if ( array_key_exists($key, $cfg) )
				return $cfg[$key];

		
		// si on arrive ici, la valeur demandée n'existe pas dans le tableau de config
		if ( $exception_if_missing )
			throw new Bootstrap\Exceptions\NotFound("Paramètre '$key' non défini");
		else
			return $defvalue;
	}
	
	
    
	/**
	 * Gérer les inclusions PHP
	 *
	 * @param \Ppast\App_Framework\Name_space $ns Namespace courant app/ctx/page
	 * @throws \Ppast\App_Framework\Bootstrap\Exceptions\NotFound Exception levée si la ressource référencée n'existe pas 
	 */
	static function doIncludes(Name_space $ns)
	{
		$kinds = array(Manifest::MANIFEST_APP, Manifest::MANIFEST_CONTEXT, Manifest::MANIFEST_PAGE);

		foreach ( $kinds as $kind )
		{
			// includes au niveau de l'appli, contexte ou page
			$m = Manifest::getManifest($ns, $kind);
     		if ( $m && ($incls = $m->{Manifest::INCLUDES}) )
			{
				if ( !is_array($incls) )
					$incls = [$incls];
				
				foreach ( $incls as $incl )
					(new Bootstrap\PageRes\PHPInclude($incl, $ns))->doInclude();
			}
		}
	}
	
	
    
	/**
	 * Générer le template dans la sortie standard
	 *
	 * @param \Ppast\App_Framework\Name_space $ns Namespace courant app/ctx/page
	 * @throws \Ppast\App_Framework\Bootstrap\Exceptions\Framework Classe de base d'exceptions levées selon divers problèmes rencontrés
	 */
	static function doTemplate(Name_space $ns, Request $req)
	{
		// obtenir le template à utiliser ; rechercher dans manifest app --> ctx --> page ; pour différentes raisons, une page particulière peut nécessiter un modèle spécifique
		(new Bootstrap\PageRes\Template(self::getManifestFinalValue($ns, Manifest::TEMPLATE)))->render($ns, $req);
	}
	
	
	
	/**
	 * Sécuriser l'application
	 *
	 * @param \Ppast\App_Framework\Name_space $ns Namespace courant app/ctx/page
	 * @param bool|string $login Contient false en utilisation normale, ou 'login' ou 'logout' pour les cas particuliers d'utilisation
	 * @param array $logindata Information de login sous forme de tableau associatif
	 */
	static function doSecurity(Name_space $ns, $login = false, $logindata = array())
	{
		// créer le gestionnaire composite depuis les instances des gestionnaires de sécurité, créés depuis les manifest
		$composite = new Bootstrap\SecurityHandlers\Composite(array());
		$composite->setInnerHandlers(array_values(Bootstrap\SecurityProcessor::getSecurityHandlers($ns)));
		
		// obtenir la session ; si pb exception
		self::$session = self::_getSession($ns);

		// démarrer la session
		self::$session->start();		
        
        // valider le gestionnaire composite ; si pb exception
        Bootstrap\SecurityProcessor::doSecurity($composite, $login, $logindata);
    }
	
	
	   
	/**
	 * Traiter la requete CMD (chaine extraite de l'url) sur le namespace courant
	 *
	 * @param \Ppast\App_Framework\Name_space $ns Namespace courant app/ctx/page
	 * @return \Ppast\App_Framework\Request Renvoie la requête qui a été traitée
	 */
	static function doProcess(Name_space $ns)
	{
		// extraite manifest appli
		$mapp =  Manifest::getManifest($ns, Manifest::MANIFEST_APP);
		if ( !$mapp	)
			throw new Bootstrap\Exceptions\NotFound("Impossible d'obtenir le manifest de l'application");
		
		
		// lire nom appli
		$appname = $mapp->{Manifest::APP_NAME};
		if ( !$appname )
			throw new Bootstrap\Exceptions\NotFound("Impossible d'obtenir le nom de l'application depuis son manifest");


		// lire préfixe NS pour autoload php (ou prendre nom appli par défaut) et normaliser (pas de \ au début, mais \ à la fin)
		$app_ns = $mapp->{Manifest::APP_NAMESPACE};
		$app_ns or $app_ns = $appname;
		$app_ns = trim($app_ns, '\\') . '\\';


		// lire stratégie fournisseur de données pour le registre application persistent
		$strat = $mapp->{Manifest::APP_REGISTRY_PROVIDER};
		if ( !$strat )
			throw new Bootstrap\Exceptions\NotFound("La stratégie fournisseur de données pour le registre application n'est pas définie dans le manifest App");
		
        
        
		// initialiser l'application ; si pb exception
		Application::init(
                /* app registry provider */
                $strat, 

                /* session */
                self::$session, 

                /* paramètres appli */
                array(
                        /* contexte des commandes, facilement extrait du namespace $ns */
                        Application::P_DEFAULT_CONTEXT => self::getManifestFinalValue($ns, Manifest::DEFAULT_CONTEXT, $ns->ctx),

                        /* nom application */
                        Application::P_APP_NAME => $appname,

						/* namespace appli */
						Application::P_NAMESPACE => $app_ns,
					
						/* valeur par défaut registre volatile */
						Application::P_RUN_REGISTRY_VALUES => $mapp->{Manifest::RUN_REGISTRY_VALUES}
                )
            );
		
		
		// définir un autoload pour les commandes/pagecontainers référencés par namespace php : ns\app\ctx\page\Commands\Cmd.php
		ClassReader::registerSplAutoload($app_ns);
			
		// exécuter l'application et renvoyer la requete résultante
		$req = Application::run();

		// préparer renvoi données (fait concrètement dans fonction statique 'boot')
		Controller::prepareOutput($req);
		
		return $req;
	}
	
	
    
	/**
	 * Envoyer une requête à une autre commande
	 * 
	 * @param \Ppast\App_Framework\Request $req Requête à transférer
	 * @param string $ns Namespace vers lequel transférer la requête sous la forme app_ctx_page
	 * @param string $cmd Nom de commande
	 * @return int Renvoie le statut de l'exécution de la commande
	 */	 
	static function forward(Request $req, $ns, $cmd)
	{
		// convertir le ns en objet
		$nsobj = Name_space::fromString($ns);
		if ( !$nsobj )
			return $req->feedback("Impossible de transférer la commande vers '$ns:$cmd'", Command::CMD_ERROR);
  
  		// traiter les inclusions pour ce NS
		if ( $st = self::doIncludes($nsobj) )
			return $req->feedback("Erreur d'inclusions au transfert de la commande : '$st'", Command::CMD_ERROR);
		
		// chercher la commande en fonction de son nom
		$comm = ClassResolver::getInstance()->getCommand($req, $ns, $cmd);

		// exécuter la commande
		return $comm->execute($req);
	}
	
	
    
	/**
	 * Obtenir une querystring comprenant les valeurs nécessaires pour démarrer l'application par un lien et satisfaire les gestionnaires de sécurité
	 *
	 * Principalement, il s'agit de rajouter les jetons ou valeurs CSRF ; est appelé pendant reboot()
	 * 
	 * @param \Ppast\App_Framework\Name_space $ns Namespace courant app/ctx/page
	 */
	static function bootQueryString(Name_space $ns)
	{
		// créer le gestionnaire composite depuis les instances des gestionnaires de sécurité, créés depuis les manifest
		$composite = new Bootstrap\SecurityHandlers\Composite(array());
		$composite->setInnerHandlers(array_values(Bootstrap\SecurityProcessor::getSecurityHandlers($ns)));
		
		// augmenter éventuellement l'URL de reboot '/desk/routage.php?__namespace__=desk_desktop_index', avec par exemple CSRF
		$url = '';
		$composite->bootUrl(self::$session, $url);
		return substr($url, 1);	// ne pas tenir compte du ? initial
	}
	
    
	    
	/**
	 * Formater une erreur en HTML et arrêter l'exécution
	 *
	 * @param Throwable $e Exception à traiter
	 */
	static function handleException(\Throwable $e)
	{
        (new Bootstrap\ExceptionHandlers\ExceptionHandler())->handleException($e);
        die();
	}
    
	
	
    /**
	 * Gérer une exception liée à une mauvaise identification et arrêter le script
	 * 
	 * Dans ce cas, rediriger vers la page d'accueil de l'application avec le message d'erreur en querystring
	 *
	 * @param \Ppast\App_Framework\Bootstrap\Exceptions\Auth $e Exception à traiter
	 */
    static function handleAuthException(Bootstrap\Exceptions\Auth $e)
    {
        $loginurl = self::getManifestFinalValue(self::$namespace, Manifest::APP_URL);
        
        // si pas trouvé le paramètre pour la page de login de l'appli, traiter comme une exception
        if ( !$loginurl )
            throw $e;
        
        
        $err = urlencode($e->getMessage());

        if ( strpos($loginurl, '?') === FALSE )
            $loginurl = '/' . Bootstrap::$approot . $loginurl . "?__error__=$err";
        else
            $loginurl = '/' . Bootstrap::$approot . $loginurl . "&__error__=$err";

        header("Location: $loginurl");
        die();       
    }
	
	
    
	/**
	 * Définir racine app, par rapport à la racine web
	 *
	 * Par construction, $approot ne commence pas par / mais se termine par /, sauf si l'application est dans la racine web, auquel cas approot est strictement vide
	 *
	 * @param string $approot Chemin à définir ; les / en trop ou manquant au début et à la fin seront enlevés ou rajoutés
	 */
	static function setAppRoot($approot)
	{
		// mémoriser chemin vers l'appli ; par défaut, directement en racine www (Bootstrap::$root), mais si
		// dans un sous-dossier, en utilise $approot : $approot = repos/app_frmk/
		self::$approot = ltrim(rtrim($approot, '/') . '/', '/');
	}
	    
    
    
    /**
	 * Point d'entrée de l'application
	 *
	 * Les exceptions sont interceptées ici et traitées
	 */
    static function routage()
    {
        try
        {
            // booter depuis l'url
            self::bootFromUrl();
        }
        catch (\Throwable $e)
        {
            // traiter l'exception : affichage formaté et fin du script
            self::handleException($e);
        }
    }	
	
    
	
	/**
	 * Démarrer le bootstrap depuis une url (qui contient namespace et éventuellement commande + paramètres)
	 *
	 * @throws \Ppast\App_Framework\Bootstrap\Exceptions\NotFound Exception levée si namespace inconnu
	 */
	static function bootFromUrl()
	{
		if ( array_key_exists(Request::REQUEST_NAMESPACE, $_REQUEST) && ($ns = $_REQUEST[Request::REQUEST_NAMESPACE]) )
			self::boot($ns);
		else
			throw new Bootstrap\Exceptions\NotFound("Boot impossible, namespace inconnu");
	}
	
    
	
	/**
	 * Après une initialisation du bootstrap, rebooter l'application pour la démarrer normalement (via redirection)
	 *
	 * @param \Ppast\App_Framework\Name_space $ns Namespace courant app/ctx/page
	 * @throws \Ppast\App_Framework\Bootstrap\Exceptions\NotFound Exception levée si l'url de routage (par rapport à approot) n'est pas défini dans les manifest
	 */
	static function reboot(Name_space $ns)
	{
		// rebooter l'application
		$route = self::getManifestFinalValue($ns, Manifest::APP_ROUTE);
		if ( !$route )
			throw new Bootstrap\Exceptions\NotFound("URL de routage inconnue");
			
		
		// constituer une URL pour reboot et l'augmenter éventuellement de paramètres liés aux securityhandlers
		$url = '/' . Bootstrap::$approot . $route . '?__namespace__=' . $ns->toString() . '&__reboot__=1&' . self::bootQueryString($ns);
		header("Location: $url");
		die();
	}
	
    
	
	/**
	 * Initialiser le bootstrap
	 *
	 * - En cas d'identification ok, on reboote l'application via reboot() pour la démarrer de façon normale, notamment avec
	 *   les valeurs requises pour les gestionnaires de sécurité (csrf, etc.)
	 * - $approot peut être deviné, en postulant que login est appelé depuis une arborescence normée app/ctx/_login.php
	 *
	 * @param \Ppast\App_Framework\Name_space $ns Namespace courant app/ctx/page
	 * @param array $data Tableau associatif de valeurs d'initialisation pour les gestionnaires de sécurités
	 * @param string $approot Racine de l'application, relativement à la racine web
	 * @param bool $logout Définir à true pour indiquer qu'on ferme l'application
	 * @throws \Ppast\App_Framework\Bootstrap\Exceptions\NotFound Exception levée si namespace inconnu
	 */
    // si pb exception
	static function login($ns, $data, $approot = '', $logout = false)
	{
        try
        {
            // approot peut être deviné, en postulant que login est appelé depuis une arborescence normée app/ctx/_login.php
            // si ce n'est pas le cas, il faudra préciser explicitement $approot
            if ( !$approot )
                // (PHP_SELF : /repos/app_frmk/nouser/ctx/_login.php)
                $approot = dirname($_SERVER['PHP_SELF'], 3);
            
            self::setAppRoot($approot);

            
            // init
            self::init($ns);

            if ( !self::$namespace )
                throw new Bootstrap\Exceptions\NotFound("Impossible d'obtenir une construction du namespace");


            // sécuriser la connexion avec création connexion ; si pb parametres security handlers, exception NotFound ; la connexion est 
            // simplement  initialisée, l'authentification (user/passwd) nécessite le reboot de l'application
            self::doSecurity(self::$namespace, $logout ? 'logout':'login', $data);

            // rebooter l'appli maintenant (sauf si on veut faire logout)
            if ( !$logout )
                self::reboot(self::$namespace);	
        }
        catch (\Throwable $e)
        {
            // traiter l'exception : affichage formaté et fin du script
            self::handleException($e);
        }
	}
	
    
	
	/**
	 * Fermer l'application
	 *
	 * @param \Ppast\App_Framework\Name_space $ns Namespace courant app/ctx/page
	 * @param string $approot Racine de l'application, relativement à la racine web
	 */
	static function logout($ns, $approot = '')
	{
		self::login($ns, array(), $approot, true);
	}
    
    
    
	/**
	 * Construire la page depuis un namespace chaîne app_ctx_page
	 *
	 * @param string $ns
	 * @throws \Ppast\App_Framework\Bootstrap\Exceptions\NotFound Exception levée si namespace inconnu
	 */
	static function boot($ns)
	{
		// déterminer chemin d'installation de l'appli ; par construction, BOOT est appelé depuis routage, toujours placé 
        // en racine de l'appli (PHP_SELF : /repos/app_frmk/nouser/routage.php)
        self::setAppRoot(dirname(rtrim(dirname($_SERVER['PHP_SELF']), '/')));
        
        // init
		self::init($ns);
				
		// vérifier que le namespace a pu être déchiffré
		if ( !self::$namespace )
			throw new Bootstrap\Exceptions\NotFound("Impossible d'obtenir une construction du namespace");



		// faire les inclusions ; si pb, exception
		self::doIncludes(self::$namespace);
		
		
		try
        {
            // assurer la sécurité de la connexion ; si pb exception ; si pb auth après reboot, exception Auth et traiter spécifiquement
            self::doSecurity(self::$namespace);
        }
        catch(Bootstrap\Exceptions\Auth $e)
        {
            // regarder si reboot ; si oui, rediriger les erreurs vers url de login
            if ( self::$namespace && self::$session && (self::$session->requestInterface->__reboot__ == '1') )
                self::handleAuthException($e);
            
            // si non traitée, lever à nouveau l'exception
            throw $e;
        }
			
		
        // traiter la requete ; si pb exception
		$req = self::doProcess(self::$namespace);

        
		// effectuer le rendu immédiat de la commande (notamment pour xmlhttp ou download)
		StdoutController::flush($req);
		
        
		// tester si pas arrêt processus demandé (xmlhttp ou download)
		if ( !$req->getCommand()->halt() )
			// modèle de page, lié au contexte ; si pb exception
			self::doTemplate(self::$namespace, $req);
	}
	
	// ---- DECL. PUBLIQUES ----]
}

?>