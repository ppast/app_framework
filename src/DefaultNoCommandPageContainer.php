<?php

namespace Ppast\App_Framework;




// proposer une page par défaut en cas d'absence de commande ; celle-ci renvoie systématiquement un contenu vide
class DefaultNoCommandPageContainer extends PageContainer
{
	/**
	 * Méthode contenant le code réel d'affichage
	 *
	 * @param Request $req
	 */
	function doRender(Request $req)
	{
	}
}



?>