<?php

namespace Ppast\App_Framework\OutputHandlers;

use \Ppast\App_Framework\Request;



// classe de base pour définir gestion de commande terminée
abstract class Handler
{
	// --- DECL. PROTEGEES ---

	protected $_headers = array();
	protected $_content = '';
	
	
	/**
	 * Ajouter un en-tête dans le retour de la commande
	 * 
	 * @param string $h
	 */
	protected function addHeader($h)
	{
		$this->_headers[] = $h;
	}
	
	// --- /DECL. PROTEGEES ---

	
	
	/**
	 * Réaliser le retour de la commande (ajout header et préparer le contenu à renvoyer)
	 *
	 * @param \Ppast\App_Framework\Request $req
	 */
	abstract function prepare(Request $req);
	
	
	
	/**
	 * Envoyer le contenu au client (header + contenu), préparé dans 'prepare'
	 */
	function stdoutWrite()
	{
		foreach ( $this->_headers as $h )
			header($h);
			
		echo $this->_content;
		
		// renvoi contenu différé éventuel
		$this->lazyWrite();
	}
	
	
	
	/**
	 * Obtenir les en-têtes qui seront renvoyés
	 *
	 * @return string[] Renvoie la liste des en-têtes
	 */
	function getHeaders()
	{
		return $this->_headers;
	}
	
	
	
	/**
	 * Obtenir le contenu qui sera renvoyé
	 * 
	 * @return string
	 */
	function getContent()
	{
		return $this->_content;
	}
	
	
	
	/**
	 * Obtenir contenu préparé par 'prepare'
	 *
	 * @return string
	 */
	function getOutput()
	{
		return $this->getContent();
	}
	
	
	
	/**
	 * Renvoie/rendu différé ; par défaut, ne rien faire
	 */
	function lazyWrite()
	{
	}
}

?>