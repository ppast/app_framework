<?php

namespace Ppast\App_Framework\OutputHandlers;

use \Ppast\App_Framework\Request;
use \Ppast\App_Framework\Command;



// classe pour définir gestion de commande xmlhttp terminée en PHP
final class XMLHTTP extends Handler
{
	/**
	 * Réaliser le retour (ajout header et préparer le contenu à renvoyer dans $this->_content)
	 *
	 * @param \Ppast\App_Framework\Request $req
	 */
	function prepare(Request $req)
	{
		// envoyer les en-tetes HTTP spécifiques à xml http request ; par défaut, content-type:text/javascript; charset=utf-8
		$this->addHeader("Content-Type: application/json; charset=utf-8");
		
		// désactiver le cache
		$this->addHeader("Expires: Sat, 1 Jan 2005 00:00:00 GMT");
		$this->addHeader("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
		$this->addHeader("Cache-Control: no-cache, must-revalidate");
		$this->addHeader("Pragma: no-cache");
		
		
		// obtenir valeur en retour
		$ret = $req->getReturns();
		

		// traiter selon statut d'exécution
		switch ( $req->getCommand()->getStatus() )
		{
			// si ok
			case Command::CMD_OK :
				$ret['status'] = true;
				$this->_content .= json_encode($ret);
				break;
				
			// si paramètres manquants
			case Command::CMD_INSUFFICIENT_DATA:
				$ret = array();
				$ret['status'] = false;
				$ret['message'] = "Paramètres manquants ou incorrects";
				$this->_content .= json_encode($ret);
				break;
				
			// si commande pas trouvée
			case Command::CMD_NOT_FOUND:
				$ret = array();
				$ret['status'] = false;
				$ret['message'] = "Commande inconnue : " . implode("\n", $req->getFeedback());
				$this->_content .= json_encode($ret);
				break;
				
			// si erreur interne dans la commande (CMD_ERROR)
			default:
				$ret['status'] = false;
				$ret['message'] = implode("\n", $req->getFeedback());
				$this->_content .= json_encode($ret);
		}
		
		// arrêter le processus
		$req->getCommand()->halt(true);
	}
}



?>