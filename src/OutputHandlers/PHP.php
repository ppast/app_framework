<?php

namespace Ppast\App_Framework\OutputHandlers;


use \Ppast\App_Framework\Request;



// classe pour définir gestion de commande terminée en PHP
final class PHP extends Handler
{
	/**
	 * Réaliser le retour (ajout header et préparer le contenu à renvoyer)
	 *
	 * @param \Ppast\App_Framework\Request $req
	 */
	function prepare(Request $req)
	{
		// rien à faire de particulier
		$this->addHeader("Content-Type: text/html; charset=utf-8");
	}

}


?>