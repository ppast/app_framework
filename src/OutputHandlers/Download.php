<?php

namespace Ppast\App_Framework\OutputHandlers;

use \Ppast\App_Framework\Request;
use \Ppast\App_Framework\Command;
use \Ppast\App_Framework\StdoutController;



// classe pour définir gestion de commande terminée en téléchargement
// utilisation des valeur de retour "output", "content-type", "name"
final class Download extends Handler
{
	// données de téléchargement
	public $CONTENT_TYPE = NULL;
	public $FILENAME = NULL;
	public $OUTPUT = NULL;
	public $FILE_OUTPUT = NULL;
	public $FILE_DELETE = NULL;
	public $OUTPUT_FILE = NULL;
	
	
	// --- /CONSTANTES ---
	

	/**
	 * Fonction d'aide pour définir les valeurs décrivant le téléchargement de données brutes
	 *
	 * @param \Ppast\App_Framework\Request $req Requête
	 * @param string $contentType Type de contenu du fichier téléchargé
	 * @param string $filename Nom de fichier suggéré au navigateur
	 * @param string $output Contenu à télécharger
	 * @return \Ppast\App_Framework\Request
	 */
	function setDownload(Request $req, $contentType, $filename, $output)
	{
		$this->CONTENT_TYPE = $contentType;
		$this->FILENAME = $filename;
		$this->OUTPUT = $output;
		$this->OUTPUT_FILE = false;
		return $req;
	}
	
	
	
	/**
	 * Fonction d'aide pour définir les valeurs décrivant le téléchargement d'un fichier
	 *
	 * @param \Ppast\App_Framework\Request $req Requête
	 * @param string $contentType Type de contenu du fichier téléchargé
	 * @param string $filename Nom de fichier suggéré au navigateur
	 * @param string $file Chemin vers le fichier dont le contenu doit être téléchargé
	 * @param bool $delete Indique si le fichier $file doit être supprimé à l'issue du téléchargement
	 * @return \Ppast\App_Framework\Request
	 */
	function setFileDownload(Request $req, $contentType, $filename, $file, $delete = false)
	{
		$this->CONTENT_TYPE = $contentType;
		$this->FILENAME = $filename;
		$this->FILE_OUTPUT = $file;
		$this->FILE_DELETE = $delete;
		$this->OUTPUT_FILE = true;
		return $req;
	}
	
	
	
	/**
	 * Obtenir contenu renvoyé
	 *
	 * N'est utilisé que pour tester, ne renvoie que les données, pas les en-têtes
	 *
	 * @return string
	 */
	function getOutput()
	{
		if ( $this->OUTPUT_FILE )
			return $this->_content . file_get_contents($this->FILE_OUTPUT);
		else
			return $this->_content . $this->OUTPUT;
	}
	
	
	
	/**
	 * Pour les téléchargements, envoyer le contenu de manière différée, on n'utilise pas $this->_content pour éviter de recopier inutilement les données
	 * 
	 * - Pour le téléchargement de gros fichiers, on utilise cette méthode pour écrire le contenu directement
	 * dans la sortie standard avec readfile() plutôt que de stocker le fichier dans $this->_content
	 * - Pour le téléchargement de données brutes, on recopie $this->OUTPUT	 
	 */
	function lazyWrite()
	{
		if ( $this->OUTPUT_FILE )
		{
			readfile($this->FILE_OUTPUT);
			if ( $this->FILE_DELETE )
				unlink($this->FILE_OUTPUT);
		}
		else
			echo $this->OUTPUT;
	}


	
	/**
	 * Préparer le téléchargement après exécution de la commande
	 *
	 * On prépare notamment l'envoi des en-têtes
	 *
	 * @param \Ppast\App_Framework\Request $req Requête
	 */
	function prepare(Request $req)
	{
		// si commande OK, envoyer le téléchargement
		if ( $req->getCommand()->getStatus() == Command::CMD_OK )
		{		
			// en-tête pour forcer le téléchargement et suggérer un nom
			$this->addHeader("Content-Type: " . $this->CONTENT_TYPE . "; name=\"" . $this->FILENAME . "\"");
			$this->addHeader("Content-Transfer-Encoding: binary");
			$this->addHeader("Content-Disposition: attachment; filename=\"" . $this->FILENAME . "\"");
			$this->addHeader("Expires: 0");
			$this->addHeader("Cache-Control: no-cache, must-revalidate");
			$this->addHeader("Pragma: no-cache"); 
	
			// arrêter le processus
			$req->getCommand()->halt(true);
		}
		
		// si commande en erreur, utiliser le gestionnaire de rendu par défaut (PHP)
		else
		{
			$req->getCommand()->setOutputHandler(StdoutController::OUTPUT_PHP);
			$req->getCommand()->getOutputHandler()->prepare($req);
		}
	}
}


?>