<?php

namespace Ppast\App_Framework;



// gestionnaire de lecture d'une classe (soit par namespace php, soit par système de fichiers page_commands.php)
class ClassReader
{
	// --- DECL. PRIVEES ---
	protected $_nsPrefix;

	
	
	/**
	 * Constructeur 
	 *
	 * @param string $ns_prefix Préfixe de namespace pour le projet applicatif
	 */
	public function __construct($ns_prefix)
	{
		// prefixe namespace de l'application
		$this->_nsPrefix = $ns_prefix;
	}
	
	
	
	
	/**
	 * Tenter d'obtenir la classe à inclure par réflection pour répondre au namespace PHP et à la commande ($cl) demandée
	 *
	 * @param \Ppast\App_Framework\Name_space $ns Namespace courant app/ctx/page
	 * @param string $cl Commande recherchée
	 * @param string $cltype Type de classe sous forme de chaîne (Command ou PageContainer)
	 * @return ReflectionClass|string Renvoie une classe de réflection si tout s'est bien passé, une chaîne avec message d'erreur sinon
	 */
	protected function _getNamespacedReflectionClass(Name_space $ns, $cl, $cltype)
	{
		// convertir namespace app_ctx_page en app\ctx\page\
		// nom de classe à instancier
		$c = $this->_nsPrefix . $ns->toNamespace() . ucfirst($cltype) . '\\' . ucfirst($cl);
		
		// préfixer par prefixe namespace de l'application ; rechercher dans namespace nsprefix\app\ctx\page\Command\$cl (ou nsprefix\app\ctx\page\PageContainer\$cl)
		// par construction, le fichier a sa première lettre en majuscule
		if ( class_exists($c) )
			return new \ReflectionClass($c);
		else
			return "Commande '$c' introuvable par namespace";
	}
	
	
	
	/**
	 * Obtenir une classe traitante pour cette requête, en tentant l'instanciation sur un namespace donné
	 *
	 * @param string $ns Namespace courant app_ctx_page sous forme de chaîne
	 * @param string $cl Commande recherchée
	 * @param string $cltype Type de classe sous forme de chaîne (Command ou PageContainer)
	 * @return string|object Renvoie une chaîne décrivant l'erreur, ou l'objet créé avec succès
	 */
	function getClass($ns, $cl, $cltype)
	{
		// construire le ns
		if ( $n = Name_space::fromString($ns) )
		{
			// tenter obtention de classe depuis namespace PHP
			$st = $this->_getNamespacedReflectionClass($n, $cl, $cltype);
			return $st;
		}
		else
			return "Namespace indéchiffrable en /app/ctx/page";
	}
	
	
	
	/**
	 * Enregistrer un chargeur de classes
	 *
	 * @param string $app_ns Préfixe de namespace pour le projet applicatif
	 */
	public static function registerSplAutoload($app_ns)
	{
		spl_autoload_register(function ($class) use ($app_ns){

			// base directory for the namespace prefix
			$base_dir = Bootstrap::$root . '/' . Bootstrap::$approot;

			// does the class use the namespace prefix?
			$len = strlen($app_ns);
			if (strncmp($app_ns, $class, $len) !== 0) {
				// no, move to the next registered autoloader
				return;
			}

			// get the relative class name
			$relative_class = substr($class, $len);

			// replace the namespace prefix with the base directory, replace namespace
			// separators with directory separators in the relative class name, append
			// with .php
			$file = $base_dir . str_replace('\\', '/', $relative_class) . '.php';
			
			
			// if the file exists, require it
			if (file_exists($file)) {
				require $file;
			}
		});
	}	
}


?>