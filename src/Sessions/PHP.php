<?php

namespace Ppast\App_Framework\Sessions;


// classe de session PHP
class PHP extends Session
{
    protected $browserStorage = NULL;
    protected $request = NULL;
    protected $fileUploadRequest = NULL;
    
    
	/**
	 * Accesseur pour stockage session
	 *
	 * @param string $k Nom de valeur à obtenir dans la session
	 * @return mixed
	 */
	function get($k)
	{
		if ( array_key_exists($k, $_SESSION) )
			return $_SESSION[$k];
		else
			return null;
	}
	
	
	
	/**
	 * Accesseur pour stockage session
	 *
	 * @param string $k Nom de valeur à définir dans la session
	 * @param mixed $v Valeur à définir
	 */
	function set($k, $v) { return $_SESSION[$k]=$v; }
	
	
	
	/**
	 * Accesseur pour stockage session
	 *
	 * @param string $k Nom de valeur à effacer dans la session
	 */
	function delete($k)
	{
		unset($_SESSION[$k]);
	}
	

	
	/**
	 * Obtenir une interface au stockage du navigateur (cookies)
	 * 
	 * @return \Ppast\App_Framework\BrowserInterfaces\Cookies
	 */
	function getBrowserStorageInterface()
	{
        if ( !$this->browserStorage )
            $this->browserStorage = new \Ppast\App_Framework\BrowserInterfaces\Cookies();
        
        return $this->browserStorage;
	}
	
	
	
	/**
	 * Obtenir une interface pour requete du navigateur
	 *
	 * @return \Ppast\App_Framework\BrowserInterfaces\Request
	 */
	function getBrowserRequestInterface()
	{
        if ( !$this->request )
		  $this->request = new \Ppast\App_Framework\BrowserInterfaces\Request();
        
        return $this->request;
	}


	
	/**
	 * Obtenir une interface pour fichiers téléchargés
	 *
	 * @return \Ppast\App_Framework\BrowserInterfaces\FileUploadsRequestPHP
	 */
	function getFileUploadsRequestInterface()
	{
        if ( !$this->fileUploadRequest )
            $this->fileUploadRequest = new \Ppast\App_Framework\BrowserInterfaces\FileUploadsRequestPHP();
        
        return $this->fileUploadRequest;
	}

	

	/**
	 * Démarrer la session
	 */
	function start() 
	{
		// nommer la session
		session_name($this->name);
		
		// ouvrir la session
		session_start();
	}
	
	
	
	/**
	 * Détruire la session
	 */
	function destroy()
	{
		session_destroy();
	}
	
	
	
	/**
	 * Enumérer les noms de valeurs dans la session
	 *
	 * @return string[] Renvoie une liste de noms de valeurs
	 */
	function enum(){ return array_keys($_SESSION); }
}


?>