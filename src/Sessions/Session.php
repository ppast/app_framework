<?php

namespace Ppast\App_Framework\Sessions;



// classe encapsulatrice de session
abstract class Session
{
	// [---- DECL. STATIQUES ----

	// ---- DECL. STATIQUES ----]
	
	
	/**
	 * Accesseur pour stockage session
	 *
	 * @param string $k Nom de valeur à obtenir dans la session
	 * @return mixed
	 */
	abstract function get($k);

	
	
	/**
	 * Accesseur pour stockage session
	 *
	 * @param string $k Nom de valeur à définir dans la session
	 * @param mixed $v Valeur à définir
	 */
	abstract function set($k, $v);
	

	
	/**
	 * Accesseur pour stockage session
	 *
	 * @param string $k Nom de valeur à effacer dans la session
	 */
	abstract function delete($k);
	

	
	/**
	 * Obtenir une interface au stockage du navigateur (cookies)
	 * 
	 * @return \Ppast\App_Framework\BrowserInterfaces\BrowserInterface
	 */
	abstract function getBrowserStorageInterface();
	
	
	
	
	/**
	 * Obtenir une interface pour requete du navigateur
	 *
	 * @return \Ppast\App_Framework\BrowserInterfaces\BrowserInterface
	 */
	abstract function getBrowserRequestInterface();
	
	
	
	/**
	 * Obtenir une interface pour fichiers téléchargés
	 *
	 * @return \Ppast\App_Framework\BrowserInterfaces\FileUploadsRequest
	 */
	abstract function getFileUploadsRequestInterface();
	
	
	
	/**
	 * Démarrer la session
	 */
	abstract function start();
	
	
	
	/**
	 * Détruire la session
	 */
	abstract function destroy();
	
	
	/**
	 * Enumérer les noms de valeurs dans la session
	 *
	 * @return string[] Renvoie une liste de noms de valeurs
	 */
	abstract function enum();


	
	// [---- DECL. PUBLIQUES ----

	// interfaces navigateur	
	public $storageInterface = NULL;
	public $requestInterface = NULL;
	public $fileUploadsInterface = NULL;
	public $name = NULL;
	
	
	
	/**
	 * Accesseur magique
	 *
	 * @param string $k Nom de valeur de session
	 * @return mixed
	 */
	function __get($k) { return $this->get($k);}
	

	
	/**
	 * Accesseur magique
	 *
	 * @param string $k Nom de valeur de session
	 * @param mixed $v Valeur
	 * @return mixed
	 */
	function __set($k, $v) { return $this->set($k, $v); }


	
	/**
	 * Constructeur
	 *
	 * @param string $name Identifiant de session
	 */
	function __construct($name)
	{
		$this->name = $name;
		$this->storageInterface = $this->getBrowserStorageInterface();
		$this->requestInterface = $this->getBrowserRequestInterface();
		$this->fileUploadsInterface = $this->getFileUploadsRequestInterface();
	}

	// ---- DECL. PUBLIQUES ----]
}


?>