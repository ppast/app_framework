<?php

namespace Ppast\App_Framework\Sessions;


// classe de session en utilisant les cookies de session (supprimés à la fermeture du navigateur ; $_SESSION non utilisé car parfois la session OVH se vide)
class Cookies extends PHP
{
    /**
     * Obtenir tableau associatif contenant la session
     *
     * @return array
     */
    function _getJson()
    {
        // lire contenu cookie de session
        $s = $this->getBrowserStorageInterface()->get($this->name);
        if ( $s )
            if ( $js = json_decode($s, true) )
                return $js;
            else
                return [];
        else
            return [];
    }
    
    
    
	/**
	 * Accesseur pour stockage session
	 *
	 * @param string $k Nom de valeur à obtenir dans la session
	 * @return mixed
	 */
	function get($k)
	{
        $s = $this->_getJson();
		if ( array_key_exists($k, $s) )
			return $s[$k];
		else
			return null;
	}
	
	
	
	/**
	 * Accesseur pour stockage session
	 *
	 * @param string $k Nom de valeur à définir dans la session
	 * @param mixed $v Valeur à définir
	 */
	function set($k, $v)
    {
        $s = $this->_getJson();
        $s[$k] = $v;
        
        $this->getBrowserStorageInterface()->set($this->name, json_encode((object)$s));
    }
	
	
	
	/**
	 * Accesseur pour stockage session
	 *
	 * @param string $k Nom de valeur à effacer dans la session
	 */
	function delete($k)
	{
        $s = $this->_getJson();
        unset($s[$k]);
        
        $this->getBrowserStorageInterface()->set($this->name, json_encode((object)$s));
	}

	

	/**
	 * Démarrer la session
	 */
	function start() 
	{
        // obtenir session ; si inexistante, la créer vide, sinon ne rien faire
        if ( is_null($this->getBrowserStorageInterface()->get($this->name)) )        
            $this->getBrowserStorageInterface()->set($this->name, '{}');
	}
	
	
	
	/**
	 * Détruire la session
	 */
	function destroy()
	{
		$this->getBrowserStorageInterface()->set($this->name, '{}');
	}
	
	
	
	/**
	 * Enumérer les noms de valeurs dans la session
	 *
	 * @return string[] Renvoie une liste de noms de valeurs
	 */
	function enum()
    {
        // lire contenu cookie de session
        return array_keys($this->_getJson());
    }
}


?>