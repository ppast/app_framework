<?php


namespace Ppast\App_Framework;



// classe pour décrire un téléchargement de fichiers
class FileUploadRequest
{
	protected $_error = NULL;
	protected $_tmp_name = NULL;
	protected $_size = NULL;
	protected $_type = NULL;
	protected $_name = NULL;
	
	
	/**
	 * Accesseur magique pour obtenir les propriétés error, tmp_name, size, type et name
	 * 
	 * @param string $k Nom de propriété
	 * @return mixed
	 */
	public function __get($k) { return $this->{"_$k"}; }
	
	
	
	/**
	 * Tester si un fichier a réellement été envoyé
	 * 
	 * @return bool Renvoie true si un chargement a réellement eu lieu
	 */
	public function uploaded() { return $this->_error == UPLOAD_ERR_OK;}
	
	
	
	/**
	 * Tester l'absence d'un chargement de fichier
	 *
	 * @return bool Renvoie true si aucun chargement n'a eu lieu
	 */
	public function no_file() { return $this->_error == UPLOAD_ERR_NO_FILE;}
	
	
	/**
	 * Tester l'absence d'erreur
	 * 
	 * @return Renvoie true si uploaded() ou no_file() renvoie true
	 */
	public function success() { return $this->uploaded() || $this->no_file(); }
	
	
	
	
	/**
	 * Constructeur
	 *
	 * @param int $error
	 * @param string $tmp_name Nom de fichier temporaire du chargement
	 * @param int $size Nombre d'octets chargés
	 * @param string $type Valeur ContentType du chargement
	 * @param string $name Nom d'origine du fichier envoyé
	 */
	public function __construct($error, $tmp_name, $size, $type, $name)
	{
		$this->_error = $error;
		$this->_tmp_name = $tmp_name;
		$this->_size = $size;
		$this->_type = $type;
		$this->_name = $name;
	}
}



?>