<?php

namespace Ppast\App_Framework;



// classe pour déléguer la gestion de la commande terminée : PHP, JSON(XMLHTTP), download
final class StdoutController
{
	// --- DECL. STATIQUES ---
	const OUTPUT_PHP = OutputHandlers\PHP::class;
	const OUTPUT_XMLHTTP = OutputHandlers\XMLHTTP::class;
	const OUTPUT_DOWNLOAD = OutputHandlers\Download::class;
	
	// --- /DECL. STATIQUES ---
	
	
	/**
	 * Obtenir une instance du gestionnaire de rendu de la commande terminée
	 *
	 * @param Request $req
	 * @return \Ppast\App_Framework\OutputHandlers\Handler
	 */
	static function getOutputHandler(Request $req)
	{
		// obtenir le nom du gestionnaire
		return $req->getCommand()->getOutputHandler();
	}
	
	
	
	/**
	 * Envoyer le contenu préparé dans la sortie standard (utile pour les commandes download ou xmlhttp)
	 *
	 * @param Request $req
	 */
	static function flush(Request $req)
	{
		self::getOutputHandler($req)->stdoutWrite();
	}
	
	
	
	/**
	 * Préparer le rendu
	 *
	 * @param Request $req
	 */
	static function prepare(Request $req)
	{
		self::getOutputHandler($req)->prepare($req);
	}
}


?>