<?php

namespace Ppast\App_Framework\Registries;



// classe registre avec cache
class Cached extends Registry
{
	/**
	 * Constructeur d'un registre avec mise en cache au fur et à mesure des lectures
	 *
	 * @param Registry $reg Registre à mettre en cache
	 */
	public function __construct(Registry $reg)
	{
		parent::__construct(new \Ppast\App_Framework\RegistryProviders\Cached($reg), $reg->defaultContext);
	}
}


?>