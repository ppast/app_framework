<?php

namespace Ppast\App_Framework\Registries;



// classe registre temporaire sans gestion du contexte
class VolatileRegistry
{
	protected $_items;
	
	
	
	/**
	 * Constructeur pour un registre volatile, uniquement pour la durée du script en cours
	 *
	 * @param array $values Liste de valeurs sans contexte, sous forme de tableau associatif
	 */
	public function __construct(array $values)
	{
		$this->_items = $values;
	}

	

	/**
	 * Méthode magique
	 * 
	 * @param string $k Clef recherchée
	 * @return mixed
	 */
	function __get($k)
	{
		return $this->get($k, NULL);
	}

	

	/**
	 * Méthode magique
	 * 
	 * @param string $k Clef à définir
	 * @param mixed $v Valeur
	 */
	function __set($k, $v)
	{
		return $this->set($k, $v);
	}
	
	
	
	/**
	 * Accesseur lecture
	 * 
	 * @param string $k Clef recherchée
	 * @param mixed $def Valeur par défaut si la clef n'est pas définie (pas de test de la valeur !)
	 * @return mixed
	 */
	function get($k, $def = NULL)
	{
		if ( !$this->test($k) )
			return $def;
		else
			return $this->_items[$k];
	}

	

	/**
	 * Accesseur écriture
	 * 
	 * @param string $k Clef à définir
	 * @param mixed $v Valeur
	 * @return mixed Renvoie $v pour chaînage
	 */
	function set($k, $v)
	{
		$this->_items[$k] = $v;
		return $v;
	}
	
	
	
	/**
	 * Tester l'existence d'une clef (pas de la valeur)
	 * 
	 * @param string $k Clef recherchée
	 * @return bool
	 */
	function test($k)
	{
		return array_key_exists($k, $this->_items);
	}
	
	
}

?>