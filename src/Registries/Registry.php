<?php


namespace Ppast\App_Framework\Registries;



// classe registre
class Registry
{
	// --- DECL. PRIVEES ---
	protected $_provider;
	public $defaultContext;
	// --- /DECL. PRIVEES ---
	
	
	// --- DECL. STATIQUES ---
	// --- /DECL. STATIQUES ---


	/** 
	 * Constructeur
	 * 
	 * @param \Ppast\App_Framework\RegistryProviders\Provider $provider Fournisseur de données pour ce registre
	 * @param string $defaultContext Contexte par défaut
	 */
	public function __construct(\Ppast\App_Framework\RegistryProviders\Provider $provider, $defaultContext)
	{
		$this->_provider = $provider;
		$this->defaultContext = $defaultContext;
	}

	

	/**
	 * Méthode magique
	 * 
	 * @param string $k Clef recherchée
	 * @return mixed
	 */
	function __get($k) { return $this->get($k, $this->defaultContext, NULL); }
	
	
	
	/**
	 * Méthode magiques
	 * 
	 * @param string $k Clef à définir
	 * @param mixed $v
	 */
	function __set($k, $v) { $this->set($k, $this->defaultContext, $v); }



	/**
	 * Accesseur lecture
	 *
	 * @param string $k Clef à rechercher
	 * @param string $ctx Contexte dans lequel rechercher la clef ; si null, le contexte par défaut précisé dans le constructeur sera utilisé
	 * @param mixed $def Valeur par défaut à renvoyer si la clef n'existe pas
	 */	 
	function get($k, $ctx, $def = NULL)
	{
		return $this->_provider->get($k, $ctx ? $ctx : $this->defaultContext, $def);
	}

	
	
	/**
	 * Accesseur écriture
	 *
	 * @param string $k Clef à définir
	 * @param string $ctx Contexte dans lequel écrire la clef ; si null, le contexte par défaut précisé dans le constructeur sera utilisé
	 * @param mixed $v
	 */	 
	function set($k, $ctx, $v)
	{
		return $this->_provider->set($k, $ctx ? $ctx : $this->defaultContext, $v);
	}

	
	
	/**
	 * Tester l'existence d'une clef (pas l'existence d'une valeur pour cette clef)
	 *
	 * @param string $k Clef à tester
	 * @param string $ctx Contexte dans lequel tester la clef
	 * @return bool
	 */	 
	function test($k, $ctx)
	{
		return $this->_provider->test($k, $ctx ? $ctx : $this->defaultContext);
	}
	
	
	
	/**
	 * Accesseur alternatif à get, où le premiere paramètre donne la clef ET le contexte dans un tableau à 2 valeurs
	 * 
	 * @param string[] Tableau à 2 valeurs, [0] = contexte, [1] = clef
	 * @param mixed $def Valeur par défaut à renvoyer si la clef n'existe pas
	 * @return mixed
	 */
	function qget($qk, $def = NULL)
	{
		if ( count($qk) == 2 )
			return $this->get($qk[1], $qk[0], $def);
		else
			return $def;
	}

	
	
	/**
	 * Accesseur alternatif à set, où le premiere paramètre donne la clef ET le contexte dans un tableau à 2 valeurs
	 * 
	 * @param string[] Tableau à 2 valeurs, [0] = contexte, [1] = clef
	 * @param mixed $v Valeur à définir
	 */
	function qset($qk, $v)
	{
		if ( count($qk) == 2 )
			return $this->set($qk[1], $qk[0], $v);
		else
			return $v;
	}
}


?>