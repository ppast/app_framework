<?php

namespace Ppast\App_Framework\Registries;



// classe pour forcer un contexte par défaut pour un registre (autrement qu'en utilisant le contexte par défaut défini
// lors de la construction du registre ; utile pour des fonctions partagées entre des contextes par défaut différents)
class EnsureContext extends Registry
{
	/**
	 * Constructeur pour un registre où le contexte par défaut est forcé
	 *
	 * Cela permet de partager une fonction utilisant le registre avec plusieurs appelants qui ne sont pas dans le même contexte
	 *
	 * @param Registry $reg Registre pour lequel on force un contexte
	 * @param string $defaultContext
	 */
	public function __construct(Registry $reg, $defaultContext)
	{
		parent::__construct(new \Ppast\App_Framework\RegistryProviders\EnsureContext($reg), $defaultContext);
	}
}


?>