<?php

namespace Ppast\App_Framework\BrowserInterfaces;



// interface pour navigateur (requete ou cookies)
abstract class BrowserInterface
{
	/**
	 * Méthode magique
	 *
	 * @param string $k 
	 * @return mixed
	 */
	function __get($k){ return $this->get($k);}
	
	
	
	/** 
	 * Méthode magique
	 * 
	 * @param string $k
	 * @param mixed $v
	 */
	function __set($k, $v){ return $this->set($k, $v);}
	
	
	
	/**
	 * Accesseur
	 *
	 * @param string $k
	 * @return mixed
	 */
	abstract function get($k);

	
	
	/**
	 * Accesseur
	 *
	 * @param string $k
	 * @param mixed $v
	 */
	abstract function set($k, $v);

	
	
	/**
	 * Nommer l'interface (GET, POST, COOKIES, FILE, etc.)
	 *
	 * @return string
	 */
	abstract function getType();
	
	
	
	/**
	 * Obtenir un tableau résumant le contenu de l'interface navigateur
	 *
	 * @return array
	 */
	abstract function toArray();
}



?>