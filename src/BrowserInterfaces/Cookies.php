<?php

namespace Ppast\App_Framework\BrowserInterfaces;



// interface pour stockage du navigateur
class Cookies extends BrowserInterface
{
	/**
	 * Accesseur
	 *
	 * @param string $k
	 * @return mixed
	 */
	function get($k){
		if ( array_key_exists($k, $_COOKIE) )
			return $_COOKIE[$k];
		else
			return null;			
	}

	
	
	/**
	 * Accesseur
	 *
	 * @param string $k
	 * @param mixed $v
	 */
	function set($k, $v)
	{
		setcookie($k, $v, 0, '/');
        $_COOKIE[$k] = $v;
		return $v;
	}
	
	
	
	/**
	 * Nommer l'interface (GET, POST, COOKIES, FILE, etc.)
	 *
	 * @return string
	 */
	function getType() { return "COOKIE"; }
	
	
	
	/**
	 * Obtenir $_COOKIE
	 *
	 * @return array
	 */
	function toArray() { return $_COOKIE; }
}



?>