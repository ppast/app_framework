<?php

namespace Ppast\App_Framework\BrowserInterfaces;

use \Ppast\App_Framework\FileUploadRequest;



// interface pour requete chargement de fichier du navigateur
class FileUploadsRequestPHP extends FileUploadsRequest
{
	// *** DECL. PRIVEES ***
	
	protected $_files = array();
	
	// *** /DECL. PRIVEES ***
	
	
	/**
	 * Constructeur
	 */
	function __construct()
	{
		foreach ( $_FILES as $k=>$file )
			$this->_files[$k] = new FileUploadRequest($file['error'], $file['tmp_name'], $file['size'], $file['type'], $file['name']);
	}
	
	
	
	/**
	 * Accesseur
	 *
	 * @param string $k
	 * @return mixed
	 */
	function get($k)
	{
		if ( array_key_exists($k, $this->_files) )
			return $this->_files[$k];
		else
			return null;
	}
	
	
	/**
	 * Accesseur
	 *
	 * @param string $k
	 * @param mixed $v
	 */
	function set($k, $v) { return $this->_files[$k] = $v;}
	
	

	/**
	 * Nommer l'interface (GET, POST, COOKIES, FILE, etc.)
	 *
	 * @return string
	 */
	function getType() { return 'FILES'; }
	
	
	
	/**
	 * Obtenir un tableau résumant le contenu de l'interface navigateur
	 *
	 * @return array
	 */
	function toArray() { return $this->_files; }
	
	
	
	/**
	 * Déplacer le fichier téléchargé
	 *
	 * @param FileUploadRequest $k Instance de description du fichier envoyé
	 * @param string $dest Chemin vers nouveau fichier
	 */
	function moveUploadedFile(FileUploadRequest $k, $dest)
	{
		return move_uploaded_file($k->tmp_name, $dest);
	}
}


?>