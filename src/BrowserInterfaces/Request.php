<?php

namespace Ppast\App_Framework\BrowserInterfaces;



// interface pour requete du navigateur
class Request extends BrowserInterface
{
	// *** DECL. PRIVEES ***
	
	protected $_req = NULL;
	
	// *** /DECL. PRIVEES ***
	
	
	/**
	 * Constructeur
	 */
	function __construct()
	{
		$this->_req = ($this->getType() == 'GET') ? $_GET : $_POST;
	}
	
	
	
	/**
	 * Accesseur
	 *
	 * @param string $k
	 * @return mixed
	 */
	function get($k)
	{
		if ( array_key_exists($k, $this->_req) )
			return $this->_req[$k];
		else
			return null;			
	}
	
	
	
	/**
	 * Accesseur
	 *
	 * @param string $k
	 * @param mixed $v
	 */
	function set($k, $v) { return $this->_req[$k] = $v;}

	
	
	/**
	 * Nommer l'interface (GET, POST, COOKIES, FILE, etc.)
	 *
	 * @return string
	 */
	function getType() { return $_SERVER['REQUEST_METHOD']; }
	
	
	
	/**
	 * Obtenir un tableau résumant le contenu de l'interface navigateur ($_GET ou $_POST)
	 *
	 * @return array
	 */
	function toArray() { return $this->_req; }
}

?>