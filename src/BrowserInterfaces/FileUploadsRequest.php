<?php

namespace Ppast\App_Framework\BrowserInterfaces;

use \Ppast\App_Framework\FileUploadRequest;



// interface pour upload de fichier
abstract class FileUploadsRequest extends BrowserInterface
{
	/**
	 * Déplacer le fichier téléchargé
	 *
	 * @param FileUploadRequest $k Instance de description du fichier envoyé
	 * @param string $dest Chemin vers nouveau fichier
	 */
	abstract function moveUploadedFile(FileUploadRequest $k, $dest);
}


?>