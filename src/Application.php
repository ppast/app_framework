<?php

namespace Ppast\App_Framework;



// classe pour application : centralisation de variables globales (registres, etc.)
final class Application
{
	// --- DECL. PRIVEES ---
	private function __construct() {} // pas d'instanciation publique possible, classe uniquement statique
	// --- /DECL. PRIVEES ---



	// --- DECL. STATIQUES ---
	public static $appRegistry;
	public static $runRegistry;


	// constantes pour App_Frmk_Application
	static $K_REQUEST = "__Application__::__Request__";	
	static $K_APPNAME = "__Application__::__AppName__";
	static $K_SESSION = "__Application__::__Session__";	
	static $K_DEFAULT_CONTEXT = "__Application__::__DefaultContext__";
	
	// constantes paramètres appli
	const P_DEFAULT_CONTEXT = "__registry_default_context__";
	const P_APP_NAME = "__app_name__";
	const P_NAMESPACE = "__namespace__";
	const P_RUN_REGISTRY_VALUES = "__run_reg_values__";
	
	
	
	/**
	 * Initialiser l'application
	 *
	 * @param RegistryProviders\Provider $appRegistryProvider Stratégie de fournisseur de données pour le registre application 
	 * @param Sessions\Session $session Session en cours
	 * @param array $params Paramètres supplémentaires d'initialisation
	 * @return null|string Renvoie null si initialisation ok, une chaîne avec message d'erreur sinon
	 */
	public static function init(RegistryProviders\Provider $appRegistryProvider, Sessions\Session $session, $params)
	{
		// vérifier présence paramètres
		if ( !array_key_exists(self::P_DEFAULT_CONTEXT, $params) )
			return "Paramètre P_DEFAULT_CONTEXT absent";
		if ( !array_key_exists(self::P_APP_NAME, $params) )
			return "Paramètre P_APP_NAME absent";

		
		// lire les valeurs par défaut du registre volatile, ou créer une liste vide si indéfini
		$runreg_values = array_key_exists(self::P_RUN_REGISTRY_VALUES, $params) ? $params[self::P_RUN_REGISTRY_VALUES] : null;
		
		if ( is_null($runreg_values) )
			$runreg_values = [];
		
		
		self::$appRegistry = new Registries\Application($appRegistryProvider, $params[self::P_DEFAULT_CONTEXT]);
		self::$runRegistry = new Registries\VolatileRegistry($runreg_values);
		
								
		// initialisations internes
		ClassResolver::getInstance(array_key_exists(self::P_NAMESPACE, $params)?$params[self::P_NAMESPACE]:'');
		self::$runRegistry->set(self::$K_APPNAME, $params[self::P_APP_NAME]);
		self::$runRegistry->set(self::$K_DEFAULT_CONTEXT, $params[self::P_DEFAULT_CONTEXT]);
		self::$runRegistry->set(self::$K_SESSION, $session);
		
		// init OK
		return NULL;
	}
	
	
	
	/**
	 * Exécuter l'application
	 *
	 * @return Request Renvoie l'objet requête qui a été traité
	 */
	public static function run()
	{
		return Controller::run();
	}
	

	
	/**
	 * Invoquer un container de page pour générer le rendu d'affichage
	 */
	public static function render()
	{
		Controller::render();
	}
	
	// --- /DECL. STATIQUES ---
}

?>