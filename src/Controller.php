<?php

namespace Ppast\App_Framework;



// controleur de l'application
final class Controller
{
	/**
	 * Préparer le rendu de la commande (utile pour xmlhttp ou download)
	 * 
	 * L'envoi dans la sortie standard ne se produit que plus tard dans Bootstrap::boot() par un appel à StdoutController::flush()
	 *
	 * @param Request $req
	 */
	static function prepareOutput(Request $req)
	{
		StdoutController::prepare($req);
	}
	
	
	
	/**
	 * Exécuter la commande correspondant à la requête
	 * 
	 * @return Request Renvoie l'objet qui a été instancié et executé
	 */
	static function run()
	{
		// créer une requête depuis la session
		$req = Application::$runRegistry->set(Application::$K_REQUEST, new Request(Application::$runRegistry->get(Application::$K_SESSION)));
		$cmd = ClassResolver::getInstance()->getRequestCommand($req);
		$cmd->execute($req);
		
		
		return $req;
	}
	
	
	
	/** 
	 * Déterminer un container de page approprié pour cette requête et lui déléguer le rendu
	 */
	static function render()
	{
		$req = Application::$runRegistry->get(Application::$K_REQUEST);
		$page = ClassResolver::getInstance()->getRequestPageContainer($req);
		$page->render($req);
	}
}


?>