<?php

namespace Ppast\App_Framework;



// clase pour gérer un morceau de page ; on définit des paramètres de requête obligatoires
abstract class PageContainer 
{
	/**
	 * Tester si le code de page est exécutable sur une requête (paramètres nécessaires fournis) et ses éventuels résultats
	 *
	 * @param Request $req
	 * @return bool
	 */
	function isValid(Request $req)
	{
		return true;
	}
	
	
	
	/**
	 * Méthode abstraite à définir dans les classes héritant de PageContainer pour réaliser l'affichage
	 *
	 * @param Request $req
	 */
	abstract function doRender(Request $req);


	
	/**
	 * Effectuer l'affichage, en testant avant les paramètres
	 *
	 * @param Request $req
	 */
	function render(Request $req)
	{
		if ( $this->isValid($req) )
			$this->doRender($req);
		else
			echo "Param&egrave;tres insuffisants (" . get_class($this) . ")";
	}


	
	/**
	 * Déléguer le traitement de l'affichage à une autre page
	 *
	 * @param Request $req
	 * @param string $ns Namespace sous la forme app_ctx_page
	 * @param string $cmd Nom de la commande associée au PageContainer
	 */
	function forward(Request $req, $ns, $cmd)
	{
		// chercher la commande en fonction de son nom
		$comm = ClassResolver::getInstance()->getPageContainer($req, $ns, $cmd);
		return $comm->render($req);
	}
}



?>