<?php

if ( !empty($_REQUEST['app']) )
{
	
    $json = json_decode(file_get_contents(__DIR__ . '/res/patch.json'));

	if ( !$json )
        $error = "Patch introuvable";
    else
    {
        // préparer dossier
        $tmp_path = sys_get_temp_dir() . '/' . uniqid('app_frmk');
        if ( !mkdir($tmp_path) )
            $error = "Impossible de créer le dossier temporaire";
        else
            try
            {
                // copier le dossier modèle
                shell_exec("cp -r " . __DIR__ . "/res/* $tmp_path/");
                                
                // supprimer patch
                unlink("$tmp_path/patch.json");
                
                // traiter le patch
                foreach ( $json->files as $file )
                {
                    $f = file_get_contents($tmp_path . '/' . $file->name);
                    if ( !$file->camelCase )
                        foreach ( $file->expand as $var )
                            $f = str_replace("%%$var%%", $_REQUEST[$var], $f);
                    else
                        foreach ( $file->expand as $var )
                            $f = str_replace("%%$var%%", strtoupper(substr($_REQUEST[$var],0,1)) . substr($_REQUEST[$var],1), $f);
                    
                    $fres = fopen($tmp_path . '/' . $file->name, 'w');
                    fwrite($fres, $f);
                    fclose($fres);
                }
                

                // renommer les dossiers APP et CTX
                rename("$tmp_path/app/ctx", "$tmp_path/app/" . $_REQUEST['ctx']);
                rename("$tmp_path/app", "$tmp_path/" . $_REQUEST['app']);
                
                // renommer le fichier webadmin
                rename("$tmp_path/webadmin/data/config_profiles/profile.php", "$tmp_path/webadmin/data/config_profiles/" . $_REQUEST['webadmin'] . '.php');
                
                
                // créer un fichier zip
                $ztmp = tempnam(sys_get_temp_dir(), 'z_app_frmk') . '.zip';
                shell_exec("cd $tmp_path; zip -r $ztmp *");
                
                // le télécharger
				header("Content-Type: application/zip; name=\"" . $_REQUEST['app'] . ".zip\"");
				header("Content-Disposition: attachment; filename=\"" . $_REQUEST['app'] . ".zip\"");
				header("Expires: 0");
				header("Cache-Control: no-cache, must-revalidate");
				header("Pragma: no-cache"); 
                
				readfile($ztmp);
				unlink($ztmp);
                $die = true;
            }
            finally
            {
                shell_exec("rm -rf $tmp_path");
                if ( $ztmp )
                {
                    unlink($ztmp);
                    if ( $die ) 
                        die();
                }
            }            
    }
}


?><!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>Outils de déploiement App_Frmk</title>
    <style>
        body{
            font-family: Gotham, "Helvetica Neue", Helvetica, Arial, "sans-serif";
            font-size:14px;
            margin-left:3em;
            margin-right: 3em;
        }
        
        h1 {
            text-align: center;
        }
        
        h2 {
            color:navy;
            margin-top:1.5em;
            border-top:1px dashed lightgray;
            padding-top:1.5em;
        }
        
        h2 + div {
            border-left: 3px solid lightgray;
            padding-left:0.5em;
        }
        
        form > h2:first-child {
            border-top:0;

        }
        
        h3 {
            color:red;
            margin-top:1.5em;
        }
        
        form input {
            background-color: whitesmoke;
            border:1px solid darkgray;
            box-shadow: 0 0 4px lightgray;
            height:1.8em;
        }
        
        form input[type="text"]{
            padding-left:0.2em;
            padding-right:0.2em;
        }
        
        form input[type="submit"]{
            background-color: steelblue;
            border:1px solid dimgray;
            color:white;
            height:2.2em;
        }
        
        form label {
            width:10em;
            text-align: right;
            margin-right:1em;
            display: inline-block;
        }
        
        form div.chemins label {
            width:15em;
        }
        
        form div.actions {
            margin-bottom: 2em;
        }
        
        form p span.note {
            font-size:0.9em;
            font-style: italic;
            
        }
        
        
        form div.chemins input[type='text'] {width:20em;}
        form div.application input[type='text'],
        form div.webadmin input[type='text'] {width:25em;}
    </style>
</head>

<body>
    <h1>Outil de déploiement App_Framework</h1>
    <form method="post" action="index.php">
        
        <h3><?php if ( $error ) echo htmlentities($error); ?></h3>
        
        <h2>Chemins</h2>
        <div class="chemins">
            <p><label for="path">Chemin d'installation appli : </label><input required type="text" id="path" name="path" placeholder="/demo/" value="<?php echo $_REQUEST['path'];?>"></p>
            <p><label for="libc">Chemin librairie composer : </label><input required type="text" id="libc" name="libc" placeholder="/libc/" value="<?php echo $_REQUEST['libc'];?>"></p>
            <p><label></label><span class="note">Requis : net-tools/core, net-tools/js-core, ppast/app_framework</span></p>
        </div>
        
        <h2>Application</h2>
        <div class="application">
            <p><label for="app">Nom application : </label><input type="text" id="app" required name="app" placeholder="desk" value="<?php echo $_REQUEST['app'];?>"></p> 
            <p><label for="ctx">Contexte : </label><input type="text" id="ctx" required name="ctx" placeholder="android" value="<?php echo $_REQUEST['ctx'];?>"></p>
            <p><label for="session">Nom session : </label><input type="text" id="session" required name="session" placeholder="session" value="<?php echo $_REQUEST['session'];?>"></p> 
            <p><label for="user">Utilisateur : </label><input type="text" id="user" required name="user" placeholder="admin" value="<?php echo $_REQUEST['user'];?>"></p>
            <p><label for="password">Mot de passe : </label><input type="text" id="paswword" required name="password" placeholder="1234" value="<?php echo $_REQUEST['password'];?>"></p>
        </div>
        
        <h2>Webadmin</h2>
        <div class="webadmin">
            <p><label for="webadmin">Nom profil : </label><input type="text" id="webadmin" required name="webadmin" placeholder="webadmin_profile" value="<?php echo $_REQUEST['webadmin'];?>"></p> 
            <p><label for="host">Nom d'hôte : </label><input type="text" id="host" required name="host" placeholder="http://www.host.tld" value="<?php echo $_REQUEST['host'];?>"></p> 
        </div>
        
        <h2>Actions</h2>
        <div class="actions">
            <input type="submit" value="Générer le dossier">
        </div>
    </form>
</body>
</html>