<?php

\Ppast\Webadmin\Config\Profile::setup('%%webadmin%%', array(

	// *** DROITS ***
	'PROFILE_GRANT'	=> array(\Ppast\Webadmin\Config\Users::$SUPERUSER),		// ce profil est accessible en étant superuser
	
	
	// **** APP ****	
	'APP__URL' => '%%host%%%%path%%%%app%%/%%ctx%%/_login.php',		// URL d'ouverture de l'application


	// **** OPT ****
	'PROFILE_OPT' => true,											// ce profil active l'optimisation
    'OPT__ROOT' => '%%path%%',          // racine 
	'OPT__INCLUDE_FOLDERS' => array('%%app%%'),		                // dossiers concernés par l'optimisation JS/CSS (pas de slash initial ni final !)
	'OPT_JS__WWW' => '%%host%%',                             		// chemin vers racine site (utilisé pour minify JS par api google)
	'OPT_JS__BATCH' => 10,											// nombre de fichiers JS à optimiser par batch
	'OPT_JS__ECMASCRIPT' => 'ECMASCRIPT5_STRICT',					// version ECMASCRIPT du profil
	'OPT_VERSIONNING' => true,										// ce profil active le versionning de fichiers statiques
	'OPT_VERSIONNING__FILES' => array(								// énumérer les fichiers concernés par le versionning
					'/%%app%%/%%ctx%%/_login.php'
				),					
	'OPT_VERSIONNING__PATTERN' => '/\\.min\\.(js|css)\\?v[X0-9]+/',	// pattern pour identifier les ressources à versionner
	'OPT_VERSIONNING__REPLACE' => '.min.$1?%',					// chaine de remplacement pour les ressources à versionner identifiées par le pattern
	

	// **** SEC ****
	'PROFILE_SEC' => true,											// ce site est sécurisé par CHMOD
    'SEC__ROOT' => '%%path%%',                                      // racine 
	'SEC__FOLDERS' => array('%%app%%'),			             	    // dossiers concernés par la sécurisation du profil (pas de slash initial ni final !)
	'SEC__TEST_FOLDER' => "/%%app%%",				                // dossier témoin pour activation/désactivation sécurité profil
	'SEC__METHOD' => "intelligent",									// méthode de sécurisation : par PHP glob (intelligent) ou SHELL (scripts)
	'SEC__RW_FOLDERS' => array(										// dossiers exclus et donc en RW du verrouillage profil
			'/%%app%%/business_data/_private',
			'/%%app%%/_cache'
		),
		
		
	// **** PLUGINS ****
	'PLUGINS' => array(												// liste des plugins de gestion
			),					


	// **** APPLI FRAMEWORK ****											
	'APP_FRMK_FRAMEWORK' => true,									// on utilise le framework (activation/désactivation mode optimisé et paramètres config globaux)
	'APP_MANIFEST' => "/%%app%%/manifest.php",	                    // manifest appli
    'APP_ROOT' => '%%path%%'                                        // racine appli
));

?>