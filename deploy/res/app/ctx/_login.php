<?php
define('K_NETTOOLS_INIT_LOCALE', 'fr_FR.utf8');

$root = $_SERVER['DOCUMENT_ROOT'];
include_once "{$root}%%libc%%vendor/autoload.php";
include_once "../manifest_includes.php";


use Nettools\Core\Helpers\SecurityHelper;
use Nettools\Core\Helpers\RequestSecurityHelper;
use \Ppast\App_Framework\Bootstrap;



header("Content-Type: text/html; charset=utf-8");

/*
if ( $_GET['__error__'] )
    echo "<code>" . $_GET['__error__'] . '</code>';
else
    Bootstrap::login('%%app%%_%%ctx%%_index', array(\Ppast\App_Framework\Bootstrap\SecurityHandlers\User::USER_SH_NAME=>'%%user%%', \Ppast\App_Framework\Bootstrap\SecurityHandlers\Users::USERS_SH_USER_PASSWORD=>hash('sha256','%%password%%')), '%%path%%');
*/


// si soumission formulaire
if ( strtolower($_SERVER['REQUEST_METHOD']) == 'post' )
{
	// sécuriser
	SecurityHelper::sanitize_array($_POST);
	
	
	$u = $_POST['f_login'];
	$p = $_POST['f_password'];
	$t = $_POST['f_token'];

	
	// vérifier USER/PASSWD
	if ( RequestSecurityHelper::checkFormToken($t) )
		// initialiser session en communiquant le USER et passwd ; si pb init exception gérée dans la librairie ; sinon user/passwd validé après reboot ; si pb redirection ici
		Bootstrap::login('%%app%%_%%ctx%%_index', array(\Ppast\App_Framework\Bootstrap\SecurityHandlers\User::USER_SH_NAME => $u, \Ppast\App_Framework\Bootstrap\SecurityHandlers\Users::USERS_SH_USER_PASSWORD => hash('sha256',$p)), '%%path%%');
	else
	    $msg = "Problème technique pour valider l'identification.";
}


// créer token avant output
$formtoken = RequestSecurityHelper::formToken();


// erreur de Boot
if ( !empty($_GET['__error__']) )
	$msg = SecurityHelper::sanitize($_GET['__error__']);

?>
<!DOCTYPE html>
<html>
<head>
<title>Index</title>
<script src="%%libc%%vendor/net-tools/js-core/src/js-core.min.js?vXX"></script>
<script src="%%libc%%vendor/net-tools/js-core/src/js-core.i18n.fr.min.js?vXX"></script>
<link href="%%libc%%vendor/net-tools/ui/src/ui.min.css?vXX" rel="stylesheet" type="text/css">
<link href="%%libc%%vendor/net-tools/ui/src/ui.yellow-theme.min.css?vXX" rel="stylesheet" type="text/css">

<script>

// namespace
window.%%app%% = window.%%app%% || {};
%%app%%.%%ctx%% = %%app%%.%%ctx%% || {};

// code
%%app%%.%%ctx%%.index = {

}

</script>
</head>

<body>
    <?php
    if ( $msg )
        echo "<p style=\"color:red; font-weight:bold;\">$msg</p>";
    ?>
    <form name="f_index" method="post" action="_login.php" class="uiForm">
        <input type="hidden" name="f_token" value="<?php echo $formtoken; ?>">
        Identifiant : <input type="email" autofocus required name="f_login" size="30">
        <br>
        <br>
        Mot de passe : <input type="password" required name="f_password">
        <br>
        <br>
        <div style="color:#CCC;">Horaire poste : 
            <script>
                var dt = new Date(); 
                document.write(nettools.jscore.leadingZero(dt.getHours()) + ':' +nettools.jscore.leadingZero(dt.getMinutes()) + ':' + nettools.jscore.leadingZero(dt.getSeconds())); 
            </script> 
            / horaire serveur : <?php echo date("H:i:s"); ?>
            <script>
                var dtphp = <?php echo time(); ?>; 
                var dtjs = Math.floor(dt.valueOf()/1000); 
                if ( Math.abs(dtphp - dtjs) > 50 )
                    document.write('<span style="color:red;"> - Attention, l\'horloge n\'est pas à l\'heure !</span>');
            </script>
        </div>
        <br>
        <input type="submit" value="Valider">
    </form>
</body>
</html>



