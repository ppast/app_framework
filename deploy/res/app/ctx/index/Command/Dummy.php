<?php

namespace Myprojectns\%%app%%\%%ctx%%\index\Command;


// [--- clauses USE pour application 
use Ppast\App_Framework\Application;
use Ppast\App_Framework\Bootstrap;
use Ppast\App_Framework\PageContainer;
use Ppast\App_Framework\Request;
use Ppast\App_Framework\Command;
use Ppast\App_Framework\XMLHttpCommand;
use Ppast\App_Framework\DownloadCommand;
// clauses USE pour application ---]



// **** COMMANDES ****

class Dummy extends Command 
{
	function doExecute(Request $req)
	{
		return $req->feedback(NULL, self::CMD_OK);
	}
}


?>