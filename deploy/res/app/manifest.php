<?php

use \Ppast\App_Framework\Manifest;
use \Ppast\App_Framework\Bootstrap;



$users = array(
        '%%user%%' =>
            array(
                'name'      => '%%user%%',
                'password'  => hash('sha256','%%password%%'),
                'roles'     => 'superuser'
            )
);



Manifest::describe_app(
		array(
			/* includes au niveau APPLICATION */
			Manifest::INCLUDES => array('%%app%%/manifest_includes.php'),
			Manifest::MINIFY => false,
			Manifest::APP_NAME => 'user',
			Manifest::CONFIG => array('vendor_dir' => '%%libc%%', 'nettools_jscore_dir' => '%vendor_dir%/vendor/net-tools/js-core/src/'),
			Manifest::SESSION_CLASS => \Ppast\App_Framework\Sessions\PHP::class,
			Manifest::APP_URL => '%%app%%/%%ctx%%/_login.php',
			Manifest::APP_ROUTE => '%%app%%/routage.php',
			Manifest::FEEDBACK_HANDLER => \Ppast\App_Framework\FeedbackHandlers\Simple::class,
			Manifest::APP_REGISTRY_PROVIDER => new \Ppast\App_Framework\RegistryProviders\ArrayProvider(), 
			Manifest::RUN_REGISTRY_VALUES => array(),
			Manifest::SECURITY_HANDLERS => 
				array( 'Session', 'User', 'CSRF', ['Users', new \Ppast\App_Framework\Bootstrap\SecurityHandlers\Providers\UsersStatic($users)], ['Role', array('superuser')] )
		)
	);


?>